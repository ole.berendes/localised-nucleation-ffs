#!/usr/bin/env python3

import os
import sys
from pathlib import Path

os.environ["NUMBA_DISABLE_JIT"] = "1"


PACKAGE_PATH = Path(__file__).resolve().parents[1]
sys.path.insert(0, str(PACKAGE_PATH))
sys.path.insert(0, str(PACKAGE_PATH / "submodules" / "py-pde"))

import numpy as np

from pde import PDE, CartesianGrid, FieldCollection, ScalarField
from pde.tools.misc import estimate_computation_speed

from phasesep import (
    CahnHilliardExtendedPDE,
    CahnHilliardMultiplePDE,
    FloryHuggins2Components,
    FloryHugginsNComponents,
    FreeEnergy,
)

# from phasesep.pdes.cahn_hilliard_extended import ReactiveBC


def _get_pde_var_mob():
    """helper function creating the pde with varying mobility"""
    mob = "(c * (1 - c))"
    mu = "(c**3 - c - laplace(c))"
    term_1 = f"{mob} * laplace({mu})"
    term_2 = f"dot(gradient({mob}), gradient({mu}))"
    return PDE({"c": f"{term_1} + {term_2}"})


def main():
    """main routine testing the performance"""
    print("Reports calls-per-second [fraction compared to base] (larger is better)\n")

    grid = CartesianGrid([[0, 2 * np.pi], [0, 2 * np.pi]], [64, 64])
    scalar_field = 0.5 + 1e-2 * ScalarField.random_colored(grid, exponent=-3)
    field_collection = FieldCollection([scalar_field])

    #     reactive_bcs = [
    #         [ReactiveBC(grid, ax, upper, coeff0=0.1, coeff1=0.1) for upper in [False, True]]
    #         for ax in [0, 1]
    #     ]

    # define free energies
    fG = FreeEnergy("0.5 * c**2 * (1 - c)**2")
    fFH_1 = FloryHuggins2Components(chi=4)
    fFH_2 = FloryHugginsNComponents(2, chis=4)

    VARIANTS = {
        "simple": CahnHilliardExtendedPDE(),
        "general f(c)": CahnHilliardExtendedPDE({"free_energy": fG}),
        "inhomogeneous BCs": CahnHilliardExtendedPDE(
            {"bcs": {"phi": [{"value": "sin(y)"}, {"value": "sin(x)"}]}}
        ),
        "reactions": CahnHilliardExtendedPDE({"reaction_flux": "0.01 - 0.01 * c"}),
        "varying mobility (direct)": CahnHilliardExtendedPDE(
            {
                "mobility": "c * (1 - c)",
                "rhs_implementation": "direct",
            }
        ),
        "varying mobility (split)": CahnHilliardExtendedPDE(
            {
                "mobility": "c * (1 - c)",
                "rhs_implementation": "split",
            }
        ),
        "varying mobility (staggered)": CahnHilliardExtendedPDE(
            {
                "mobility": "c * (1 - c)",
                "rhs_implementation": "staggered",
            }
        ),
        "noise": CahnHilliardExtendedPDE({"noise_diffusion": 0.1}),
        #         "reactive_bc": {"bc_mu": reactive_bcs},
        "PDE class: simple": PDE({"c": "laplace(c**3 - c - laplace(c))"}),
        "PDE class: varying mobility": _get_pde_var_mob(),
        "Flory-Huggins (single)": CahnHilliardExtendedPDE({"free_energy": fFH_1}),
        "Flory-Huggins (multiple)": CahnHilliardMultiplePDE({"free_energy": fFH_2}),
    }

    for backend in ["numba", "numpy"]:
        print(f"Backend: {backend.upper()}")
        speed_ref = None

        for name, eq in VARIANTS.items():

            if isinstance(eq, CahnHilliardMultiplePDE):
                field = field_collection
            else:
                field = scalar_field

            if eq.is_sde:
                rhs = eq.make_sde_rhs(field, backend=backend)
            else:
                rhs = eq.make_pde_rhs(field, backend=backend)
            rhs(field.data, 0)  # initialize some caches
            speed = estimate_computation_speed(rhs, field.data, 0, test_duration=1.0)

            if speed_ref is None:
                speed_ref = speed
                speedup = 1
            else:
                speedup = speed / speed_ref
            print(f"  {name:>30}:{int(speed):>8d}  [{speedup:.2g}]")

        print()


if __name__ == "__main__":
    main()
