#!/bin/bash

export PYTHONPATH=submodules/py-pde  # path to py-pde submodule

echo 'Determine coverage of all unittests...'

./run_tests.py --unit --coverage --no_numba --parallel