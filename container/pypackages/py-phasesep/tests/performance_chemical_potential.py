#!/usr/bin/env python3 -u

import sys
import timeit
from pathlib import Path

import numpy as np  # @UnusedImport

PACKAGE_PATH = Path(__file__).resolve().parents[1]
sys.path.insert(0, str(PACKAGE_PATH))
sys.path.insert(0, str(PACKAGE_PATH / "submodules" / "py-pde"))

from phasesep.free_energies import *  # @UnusedWildImport

template_specific = """
from __main__ import np, {free_energy} 
phi = np.random.random((64,))
out = np.empty_like(phi)
f = {free_energy}()
mu = f.make_chemical_potential(backend="{backend}")
mu(phi, 0, out)  # call once for jitting
"""

template_general = """
from __main__ import np, {free_energy}, FreeEnergy
phi = np.random.random((64,))
out = np.empty_like(phi)
f = {free_energy}()
f = FreeEnergy(f.expression)
mu = f.make_chemical_potential(backend="{backend}")
mu(phi, 0, out)  # call once for jitting
"""

template_specific_ncomp = """
from __main__ import np, FloryHugginsNComponents 
phi = np.random.uniform(0., 0.5, (2, 64))
out = np.empty_like(phi)
f = FloryHugginsNComponents(3, chis=2)
mu = f.make_chemical_potential(backend="{backend}")
mu(phi, 0, out=out)  # call once for jitting
"""

template_general_ncomp = """
from __main__ import np, FloryHugginsNComponents, FreeEnergy
phi = np.random.uniform(0., 0.5, (2, 64))
out = np.empty_like(phi)
f = FloryHugginsNComponents(3, chis=2)
f = FreeEnergy(f.expression)
mu = f.make_chemical_potential(backend="{backend}")
mu(phi, 0, out=out)  # call once for jitting
"""


def main():
    """main routine testing the performance"""
    num = 1000
    print(f"Reports the number of calls per second. Larger is better.\n")

    # test several free energies with 2 components
    for free_energy in ("GinzburgLandau2Components", "FloryHuggins2Components"):
        print("{}:".format(free_energy))

        # test two backends
        for backend in ["numpy", "numba"]:
            # test specific class
            sys.stdout.write(f"  {backend} (specific): ")
            time = timeit.timeit(
                "mu(phi, 0, out)",
                setup=template_specific.format(
                    free_energy=free_energy, backend=backend
                ),
                number=num,
            )
            print(f"{int(1000 / time):>8d}")

            # test general class
            sys.stdout.write(f"  {backend} (general):  ")
            time = timeit.timeit(
                "mu(phi, 0, out)",
                setup=template_general.format(free_energy=free_energy, backend=backend),
                number=num,
            )
            print(f"{int(1000 / time):>8d}")

        print()

    # test free energies with 3 components
    print("FloryHugginsNComponents:")

    # test two backends
    for backend in ["numpy", "numba"]:
        # test specific class
        sys.stdout.write(f"  {backend} (specific): ")
        time = timeit.timeit(
            "mu(phi, 0, out=out)",
            setup=template_specific_ncomp.format(backend=backend),
            number=num,
        )
        print(f"{int(1000 / time):>8d}")

        # test general class
        sys.stdout.write(f"  {backend} (general):  ")
        time = timeit.timeit(
            "mu(phi, 0, out=out)",
            setup=template_general_ncomp.format(backend=backend),
            number=num,
        )
        print(f"{int(1000 / time):>8d}")


if __name__ == "__main__":
    main()
