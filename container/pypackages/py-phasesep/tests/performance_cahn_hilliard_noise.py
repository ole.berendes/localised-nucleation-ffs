#!/usr/bin/env python3

import os
import sys
from pathlib import Path

os.environ["NUMBA_DISABLE_JIT"] = "1"


PACKAGE_PATH = Path(__file__).resolve().parents[1]
sys.path.insert(0, str(PACKAGE_PATH))
sys.path.insert(0, str(PACKAGE_PATH / "submodules" / "py-pde"))

import numpy as np

from pde import CartesianGrid, FieldCollection
from pde.tools.misc import estimate_computation_speed

from phasesep import CahnHilliardMultiplePDE, FloryHugginsNComponents

MOBILITY_MODELS = ["const_direct", "const_correct", "scaled_diagonal", "scaled_correct"]


def main():
    """main routine testing the performance"""
    print("Reports calls-per-second [fraction compared to base] (larger is better)\n")

    grid = CartesianGrid([[0, 2 * np.pi]] * 2, 32)  # initialize a 2d grid
    f = FloryHugginsNComponents(3, chis=0)  # choose simple free energy
    field = FieldCollection.scalar_random_uniform(2, grid, 0.1, 0.4)  # random state

    for backend in ["numba", "numpy"]:
        print(f"Backend: {backend.upper()}")
        speed_ref = None

        for mobility_model in MOBILITY_MODELS:  # compare different mobility models
            # prepare the PDE
            if mobility_model.startswith("const"):
                mobility = np.array([[1, 0.5, 1], [0.5, 2, 1], [1, 1, 3]])
            elif mobility_model.startswith("scaled"):
                mobility = np.array([1, 2, 3])
            eq = CahnHilliardMultiplePDE(
                {
                    "free_energy": f,
                    "noise_diffusion": 2,
                    "mobility": mobility,
                    "mobility_model": mobility_model,
                }
            )

            rhs = eq.make_sde_rhs(field, backend=backend)
            rhs(field.data, 0)  # initialize some caches
            speed = estimate_computation_speed(rhs, field.data, 0, test_duration=1.0)

            if speed_ref is None:
                speed_ref = speed
                speedup = 1
            else:
                speedup = speed / speed_ref
            print(f"  {mobility_model:>17}:{int(speed):>8d}  [{speedup:.2g}]")

        print()


if __name__ == "__main__":
    main()
