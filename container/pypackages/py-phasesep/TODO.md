* Add option `bcs` to CahnHilliardExtended:
	- This should be dictionary with keys 'phi', 'mu or 'flux' to replace the current
	  bc setup
* Add test with simulation using time-dependent free energy
* Re-introduce reactive boundary conditions:
    - these have been moved to a separate CahnHilliard class since the BCs of
      CahnHilliardExtended became too complicated
    - develop this in a separate branch
* Introduce GhostGrid and GhostField:
    - Ghost cells are also sometimes called halo cells
    - Extends normal grid and field classes with ghost cells (along each axis)
    - Builds on top of normal grids and fields (subclass? wrapper?)
    - Data is stored in _data_ghost on extended grid
    - Normal data attribute of fields is a view into the _data_ghost (without boundary)
    - Operators need to handle only true cells, not touching ghost cells
    - We need methods for supplying boundaries
    - we could develop this in branch of py-phasesep first
    - alternatively, we could rewrite py-pde to work like this
* Implement energy splitting for Kramer's mobility model
    - this is more realistic in multiple components
    - here, we should think about using uniformly distributed fluctuations (if possible)
      to have guaranteed bounds
    - Look at Zhu1999 for hints
    - Could we combine this with a staggered grid (only supporting fully periodic or
      fully non-periodic conditions)
* Support parallel execution for energy splitting in CahnHilliardMultiplePDE
* Add expression attribute to PDE classes
    - for this, each free energy needs to export its chemical potential as an expression
* Add PolarGrid 