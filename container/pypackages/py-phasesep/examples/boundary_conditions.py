#!/usr/bin/env python3
"""
Set diverse boundary conditions
===============================
"""

from pde import ScalarField, UnitGrid

from phasesep import CahnHilliardExtendedPDE

grid = UnitGrid([16, 16], periodic=[False, True])  # generate grid
state = ScalarField.random_uniform(grid, 0.2, 0.3)  # generate initial condition

# set boundary conditions `bc` for all axes for phi and mu fields
bc_x_left = {"derivative": 0.1}
bc_x_right = {"value": 0}
bc_x = [bc_x_left, bc_x_right]
bc_y = "periodic"
bcs = {"phi": [bc_x, bc_y], "flux": "auto_periodic_neumann"}

eq = CahnHilliardExtendedPDE({"bcs": bcs})

result = eq.solve(state, t_range=10, dt=0.005)
result.plot()
