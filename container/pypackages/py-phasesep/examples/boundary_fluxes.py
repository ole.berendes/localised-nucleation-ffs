#!/usr/bin/env python3
"""
Imposes reaction fluxes at the boundary
=======================================
"""

from pde import CartesianGrid, ScalarField

from phasesep import CahnHilliardExtendedPDE

grid = CartesianGrid([[-2, 2], [-2, 2]], [16, 16])  # generate grid
phi = ScalarField(grid, data=0.5)  # generate initial condition

# enforce flux = -exp(-5 * y**2) * phi on left side of the box
bc_x_left = {"flux_from_reaction": "-exp(-5 * y**2)"}
# the other boundary conditions enforce no flux
no_flux = {"value": 0}
bc_flux = [[bc_x_left, no_flux], no_flux]
eq = CahnHilliardExtendedPDE(parameters={"bcs": {"normal_flux": bc_flux}})

result = eq.solve(phi, t_range=10)
result.plot()
