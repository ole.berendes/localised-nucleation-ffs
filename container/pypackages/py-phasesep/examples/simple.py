#!/usr/bin/env python3
"""
Basic Cahn-Hilliard equation 
============================
"""

from pde import ScalarField, UnitGrid

from phasesep import CahnHilliardExtendedPDE

eq = CahnHilliardExtendedPDE()  # define the physics
grid = UnitGrid([16, 16])  # generate grid
state = ScalarField.random_uniform(grid, 0.2, 0.3)  # generate initial condition

result = eq.solve(state, t_range=10, dt=0.005)
result.plot()
