#!/usr/bin/env python3
"""
Single field with reactions with constants 
==========================================

This example displays how reaction fluxes may depend on (inhomogeneous) constants. The
constants must be an numpy array, giving the respective values at the grid points. A
more flexible, but slower, alternative are expressions that depend on spatial
coordinates; see :doc:`the example <chemical_reactions_inhomogeneous>`.
"""

from pde import CartesianGrid, ScalarField

from phasesep import CahnHilliardExtendedPDE

grid = CartesianGrid([(-16, 16)] * 2, 32)  # generate grid

# define the localized production_rate
production_rate = ScalarField.from_expression(grid, "exp(-(x**2 + y**2))")


# define Cahn-Hilliard equation with chemical reactions
eq = CahnHilliardExtendedPDE(
    {
        "reaction_flux": "production - 1e-2 * c",
        "expression_constants": {"production": production_rate.data},
    }
)

state = ScalarField(grid)  # generate initial condition

# simulate the system
result = eq.solve(state, t_range=10, dt=0.005)
result.plot()
