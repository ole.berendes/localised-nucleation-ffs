#!/usr/bin/env python3
"""
Concentration-dependent mobilities 
==================================

Display concentration-dependent mobilities.
"""

from pde import ScalarField, UnitGrid

from phasesep import CahnHilliardExtendedPDE

eq = CahnHilliardExtendedPDE({"mobility": "c * (1 - c)"})  # define the physics
grid = UnitGrid([16, 16])  # generate grid
state = ScalarField.random_uniform(grid, 0.2, 0.3)  # generate initial condition

result = eq.solve(state, t_range=1, dt=0.005, tracker=None)
result.plot()
