#!/usr/bin/env python3
"""
Multiple fields with reactions 
==============================
"""

from pde import FieldCollection, ScalarField, UnitGrid

from phasesep import CahnHilliardMultiplePDE, FloryHugginsNComponents, Reaction

f = FloryHugginsNComponents(num_comp=3)  # initialize free energy
# initialize a single chemical reaction
r = Reaction(stoichiometry=[1, 0, -1], reaction_flux="1e-3 * (1 - exp(-mu[0]))")
eq = CahnHilliardMultiplePDE({"free_energy": f, "reactions": [r]})

grid = UnitGrid([16, 16])  # generate grid
state = FieldCollection(
    [
        ScalarField.random_uniform(grid, 0.2, 0.3),
        ScalarField.random_uniform(grid, 0.2, 0.3),
    ]
)

result = eq.solve(state, t_range=1, dt=0.005)
result.plot()
