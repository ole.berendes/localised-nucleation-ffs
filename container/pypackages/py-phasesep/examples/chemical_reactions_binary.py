#!/usr/bin/env python3
"""
Single field with reactions 
===========================
"""

from pde import ScalarField, UnitGrid

from phasesep import CahnHilliardExtendedPDE

eq = CahnHilliardExtendedPDE({"reaction_flux": "-1e-1 * (1 - exp(-mu))"})
grid = UnitGrid([16, 16])  # generate grid
state = ScalarField.random_uniform(grid, 0.2, 0.3)  # generate initial condition

result = eq.solve(state, t_range=10, dt=0.005)
result.plot()
