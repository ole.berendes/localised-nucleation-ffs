#!/usr/bin/env python3
"""
Multiple mobility models 
========================

Displays how different implementations of the mobilities affects the dynamics in
multicomponent simulations.
"""

from pde import FieldCollection, UnitGrid

from phasesep import CahnHilliardMultiplePDE, FloryHugginsNComponents

# initialize free energy
chis = [[0, 4, 4], [4, 0, 4], [4, 4, 0]]
free_energy = FloryHugginsNComponents(num_comp=3, chis=chis)

# initialize state
grid = UnitGrid([16, 16])
state = FieldCollection.scalar_random_uniform(
    2, grid, 1 / 3 - 0.1, 1 / 3 + 0.1, labels=["Field 0", "Field 1"]
)

# iterate through different mobility models
for model in ["scaled_correct", "scaled_diagonal", "const_correct", "const_direct"]:
    eq = CahnHilliardMultiplePDE(
        {
            "free_energy": free_energy,
            "kappa": 0.5,
            "mobility": [1, 2, 3],  # different mobilities for different species
            "mobility_model": model,
        }
    )

    result = eq.solve(state, t_range=10, dt=0.001 if model == "direct" else 0.005)
    eq.phase_fields(result).plot(title=f"Mobility model: {model}")
