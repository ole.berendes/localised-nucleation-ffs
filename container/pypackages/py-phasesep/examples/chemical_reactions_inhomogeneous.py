#!/usr/bin/env python3
"""
Single field with localized reactions 
=====================================

This example displays how reaction fluxes may depend on spatial coordinates, which are
enumerated according to the chosen grid. Specifying spatial dependencies as an
expression is flexible, but implies the expression gets calculated each time step.
Faster simulations are possible with pre-calculated constants; see 
:doc:`the example <chemical_reactions_constants>`.
"""

from pde import CartesianGrid, ScalarField

from phasesep import CahnHilliardExtendedPDE

# define Cahn-Hilliard equation with chemical reactions
eq = CahnHilliardExtendedPDE({"reaction_flux": "exp(-(x[0]**2 + x[1]**2)) - 1e-2 * c"})
# production (first term) is localized to the center (small r^2 == x[0]^2 + x[1]^2)
# degradation (second term) is global

# generate grid and initial condition
grid = CartesianGrid([(-16, 16)] * 2, 32)
state = ScalarField(grid)

# simulate the system
result = eq.solve(state, t_range=10, dt=0.005)
result.plot()
