#!/usr/bin/env python3
"""
Multiple components 
===================

A phase separating three-component incompressible fluid simulated using two independent
fields with most parameters chosen as default values.
"""

from pde import FieldCollection, UnitGrid

from phasesep import CahnHilliardMultiplePDE, FloryHugginsNComponents

chis = [[0, 4, 0], [4, 0, 4], [0, 4, 0]]  # interaction terms
f = FloryHugginsNComponents(num_comp=3, chis=chis)  # initialize free energy
eq = CahnHilliardMultiplePDE({"free_energy": f, "kappa": 0.5})

grid = UnitGrid([16, 16])  # generate grid
state = FieldCollection.scalar_random_uniform(
    2, grid, 0.2, 0.3, labels=["Field 0", "Field 1"]
)

result = eq.solve(state, t_range=10, dt=0.005)
eq.phase_fields(result).plot()
