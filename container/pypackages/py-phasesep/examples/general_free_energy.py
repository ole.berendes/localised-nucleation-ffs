#!/usr/bin/env python3
"""
Free energy expressions 
=======================
"""

from pde import ScalarField, UnitGrid

from phasesep import CahnHilliardExtendedPDE, FreeEnergy

free_energy = FreeEnergy("5 * c**2 * (1-c)**2")  # define a free energy density
eq = CahnHilliardExtendedPDE({"free_energy": free_energy})

grid = UnitGrid([16, 16])  # generate grid
state = ScalarField.random_uniform(grid, 0.2, 0.3)  # generate initial condition

result = eq.solve(state, t_range=0.1, dt=0.005)
result.plot()
