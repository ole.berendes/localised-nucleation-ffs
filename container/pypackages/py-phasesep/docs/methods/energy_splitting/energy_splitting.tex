\documentclass[a4paper, 11pt, DIV13, numbers=noenddot]{scrartcl}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{xspace}
\usepackage{xifthen}
\usepackage{xcolor}
\usepackage{bm}


\newcommand{\Eqref}[1]{\mbox{Eq.\hspace{0.25em}\eqref{#1}}}
\newcommand{\Eqsref}[1]{\mbox{Eqs.\hspace{0.25em}\eqref{#1}}}

\newcommand{\ie}{\mbox{i.\hspace{0.125em}e.}\@\xspace}
\newcommand{\vect}{\bm}
\newcommand{\Nabla}{\vect\nabla}
\renewcommand{\rm}{\textnormal}
\newcommand{\pfrac}[2]{\frac{\partial #1}{\partial #2}}

\newcommand{\diff}{\mathrm{d}}
\newcommand{\dt}{\Delta t}
\newcommand{\Dt}{\diff t}
\newcommand{\DW}{\diff W}
\newcommand{\Dphi}{\diff \phi}
\newcommand{\mob}{\Lambda}
\newcommand{\LapN}{\Nabla^2_\rm{N}}

\renewcommand{\P}[1][]{\ifthenelse{\isempty{#1}}{\phi}{\phi^{(#1)}}}
\newcommand{\M}[1][]{\ifthenelse{\isempty{#1}}{\mu}{\mu^{(#1)}}}
\newcommand{\vP}[1][]{\ifthenelse{\isempty{#1}}{\vect\phi}{\vect\phi^{(#1)}}}
\newcommand{\vM}[1][]{\ifthenelse{\isempty{#1}}{\vect\mu}{\vect\mu^{(#1)}}}
\newcommand{\W}[1][]{\ifthenelse{\isempty{#1}}{\Delta W}{\Delta W^{(#1)}}}
\renewcommand{\Phi}{\vect{\phi}}
\newcommand{\Mu}{\vect{\mu}}

\newcommand{\sE}{\tilde s}
\newcommand{\sI}{\hat s}
\newcommand{\mE}{\tilde m}
\newcommand{\mI}{\hat m}

\newcommand{\todo}[1]{\textcolor{red}{TODO: #1}}

\begin{document}

\title{Numerical scheme using Energy Splitting for Cahn-Hilliard equations}
\author{David Zwicker \and Daniel A. Cogswell}

\maketitle
\tableofcontents

\section{Deterministic problem definition}
The differential equation that we want to solve reads
\begin{subequations}
\label{eqn:problem}
\begin{align}
	\partial_t \phi_i + \Nabla(\vect v \phi_i) &= \sum_j \mob_{ij}\Nabla^2 \mu_j + s_i(\Phi, \Mu, t) 
	\label{eqn:problem_phi}
\\
	\mu_i &= m_i(\Phi) - \sum_j \kappa_{ij} \Nabla^2 \phi_j
	\label{eqn:problem_mu}
	\;,
\end{align}
\end{subequations}
where $s_i(\Phi, \Mu, t)$ and $m_i(\Phi)$ are non-linear functions that define the physics of the system and $\vect v$ is a velocity field that describes advection.
In particular, $s_i(\Phi, \Mu, t)$ can be interpreted as the reaction rate as a function of the local concentrations~$\Phi$ and chemical potentials~$\Mu$, while $m_i(\Phi) = \partial_{\phi_i}f(\Phi)$ is the local part of the chemical potential obtained from the free energy density $f(\Phi)$.
Note that we here use a definition where $\kappa$ is generally positive, in contrast to the usual definition employed in Flory-Huggins, where $\kappa_{ij} = \lambda^2\chi_{ij}$.

We solve the partial differential equation given in \Eqref{eqn:problem} using the method of lines, where both space and time are discretized equidistantly.
While the spatial differential operator are discretized by central finite differences, we explorer different options for dealing with the temporal derivative.
In all cases, the goal is to advance the solution from time $t_0$ to $t_* = t_0 + \dt$, i.e., we seek the values $(\vP^*,\vM^*)$ given known values $(\vP[0],\vM[0])$.

\section{Deterministic explicit algorithm}
In the simplest version, we calculate the values $(\phi^*_i,\mu^*_i)$ directly from the known values $(\P[0]_i,\M[0]_i)$ using the explicit Euler scheme,
\begin{subequations}
\begin{align}
	\phi^*_i &= \P[0]_i + \dt\left[
		\sum_j\mob_{ij}\Nabla^2 \M[0]_j + s_i(\vP[0], \vM[0], t_0) - \Nabla(\vect v \P[0]_i) 
	\right]
\\
	\mu^*_i &= m_i(\vP[0]) - \sum_j \kappa_{ij} \Nabla^2 \P[0]_j
	\;,
\end{align}
\end{subequations}
These equations can be solved easily, but require rather small values of $\dt$ to converge.

\section{Energy splitting algorithm for a single component}
We start by considering a single component, which allows us to skip the index~$i$ and the sums over components.
To solve \Eqsref{eqn:problem}, we choose a mixture of an explicit and implicit time stepping, where we split the non-linear functions as
\begin{subequations}
\begin{align}
	s(\phi, \mu, t) &= \sE(\phi, \mu, t) + \sI(\phi, \mu, t)
\\
	m(\phi) &= \mE(\phi) + \mI(\phi)
	\;,
\end{align}
\end{subequations}
where the tilde and hat indicate the respective parts that are solved explicitly and implicitly.
In particular, we split the local chemical potential~$m(\phi)$ into a part corresponding to the convex free energy captured by $\mI$ (implying $\mI'(\phi)>0$), while the concave part is captured by $\mE$ (so $\mE'(\phi) < 0$).
This approximation is also known as Eyre's approximation.

The additional advective term is also treated explicitly, so the time-discretized \Eqref{eqn:problem} reads
\begin{subequations}
\begin{align}
	\frac{\phi^* - \P[0]}{\dt} + \Nabla(\vect v \P[0]) 
	&= \mob\Nabla^2 \mu^*  + \sE(\P[0], \M[0], t_0) + \sI(\phi^*, \mu^*, t_*)
\\
	\mu^* &= \mE(\P[0]) + \mI(\phi^*) - \kappa \Nabla^2 \phi^* 
	\;,
\end{align}
\end{subequations}
where we seek $(\phi^*, \mu^*)$ at time $t_* = t_0 + \dt$ given $(\P[0],\M[0])$ at time $t_0$.
Sorting terms into known and unknown contributions, we have
\begin{subequations}
\begin{align}
	\P[0]  + \dt \bigl[ \sE(\P[0], \M[0], t_0) - \Nabla(\vect v \P[0]) \bigr]
		&= \phi^* - \dt\left[\mob\Nabla^2 \mu^* + \sI(\phi^*, \mu^*, t_*)\right]
\\
	\mE(\P[0]) &= \mu^* - \mI(\phi^*) + \kappa \Nabla^2 \phi^* 
	\;.
\end{align}
\end{subequations}
We need to solve this non-linear system of equations for $(\phi^*, \mu^*)$ to advance the system by $\dt$.

\subsection{Iterative solutions of the implicit equations}
The non-linear equation can be solved iteratively, by approximating the solution $(\P^*, \M^*)$ in several steps with intermediate values $(\P[n], \M[n])$. 
The solution is obtained when the following non-linear equation is obeyed
\begin{subequations}
\label{eqn:implicit_equation}
\begin{align}
	\P[0]  + \dt \bigl[ \sE(\P[0], \M[0], t_0) - \Nabla(\vect v \P[0]) \bigr]
	&= \P[n] - \dt\left[\mob\Nabla^2 \M[n] + \sI(\P[n], \M[n], t_*)\right]
\\
	\mE(\P[0]) &= \M[n] - \mI(\P[n]) + \kappa \Nabla^2 \P[n]
	\;.
\end{align}
\end{subequations}
Note that the Laplace operator $\Nabla^2$ couples the value at a grid site to its neighbors, which makes solving the above equation difficult.
To make this coupling explicit, we introduce the discretized Laplacian
\begin{align}
	\Nabla^2\psi &\approx \LapN\psi - 2\sum_{k=1}^d \frac{1}{h_k^2} \psi
	\;,
\end{align}
where $\psi$ is a placeholder for $\phi$ and $\mu$.
Here, $\LapN\psi$ contains the sum over the neighbors, which in $d=1$ dimensions reads $\LapN\psi = h_1^{-2}(\psi_{i-1} + \psi_{i+1})$, where $i$ enumerates support points and $h$ is the spatial discretization.
Using this notation, the equations read
\begin{subequations}
\label{eqn:cahn_hilliard_timestep}
\begin{align}
	\P[0]  + \dt \bigl[ \sE(\P[0], \M[0], t_0) - \Nabla(\vect v \P[0]) \bigr]
	&= \P[n] - \dt\left[
		\mob\LapN\M[n] - \frac{2d\mob}{h^2}\M[n] + \sI(\P[n], \M[n], t_*)
	\right]
\\
	\mE(\P[0]) &= \M[n] - \mI(\P[n])
		+ \kappa \LapN\P[n]  - \frac{2d\kappa}{h^2}\P[n]
	\;.
\end{align}
\end{subequations}
To deal with the spatial coupling, we solve for the values at the approximation step $n+1$ by using the known neighbor values at step~$n$, essentially using the approximation $\LapN\P[n+1]\approx\LapN\P[n]$.
The values at the iteration $n+1$ should thus obey
%We solve the coupled equations iteratively, by introducing intermediate values $(\P[n], \M[n])$, which are initialized as $\phi^{(0)} = \P[0]$ and $\M[n] = \M[0]$.
%In the limit of large $n$ they should converge to $(\phi^*, \mu^*)$, \ie solve \Eqsref{eqn:cahn_hilliard_timestep}.
\begin{subequations}
\label{eqn:cahn_hilliard_discretized_2}
\begin{align}
	\P[0]  + \dt \bigl[ \sE(\P[0], \M[0]) - \Nabla(\vect v \P[0]) +  \mob \LapN\M[n] \bigr]
		&= \P[n+1] + \frac{2d \mob\dt}{h^2}\M[n+1] - \dt \, \sI(\P[n+1], \M[n+1], t_*)
\\
	\mE(\P[0]) - \kappa \LapN\P[n]
	&= \M[n+1] - \mI(\P[n+1]) - \frac{2d\kappa}{h^2}\P[n+1]
	\;,
\end{align}
\end{subequations}
which are now local equations for each site.
We next expand the non-linear terms on the right hand side to linear order,
\begin{subequations}
\begin{align}
	 \sI(\P[n+1], \M[n+1], t_*)
	&\approx
	\sI(\P[n], \M[n], t_*)
	+ \sI_\phi(\P[n], \M[n], t_*) (\P[n+1] - \P[n]) 
	+ \sI_\mu(\P[n], \M[n], t_*) (\M[n+1] - \M[n]) 
\\
	  \mI(\P[n+1]) 
	&\approx
	  \mI(\P[n]) + \mI'(\P[n])(\P[n+1] - \P[n])
	\;,
\end{align}
\end{subequations}
where $\sI_\phi = \partial \sI/\partial\phi$ and $\sI_\mu = \partial \sI/\partial\mu$.
We use this in \Eqsref{eqn:cahn_hilliard_discretized_2} to arrive at
\begin{subequations}
\begin{multline}
	\P[0]
	+ \dt\biggl[
		\sE(\P[0], \M[0], t_0)
     	- \Nabla(\vect v \P[0])
		+ \mob \LapN\M[n]
    		+ \sI(\P[n], \M[n], t_*)
\\
 		- \sI_\phi(\P[n], \M[n], t_*) \P[n]
		- \sI_\mu(\P[n], \M[n], t_*) \M[n]
	\biggr]
\\
	=
		\P[n+1]
		+ \frac{2d\mob\dt}{h^2}\M[n+1]
		- \dt \sI_\phi(\P[n], \M[n], t_*) \P[n+1]
		- \dt \sI_\mu(\P[n], \M[n], t_*) \M[n+1]
\end{multline}
and
\begin{equation}
	\mE(\P[0]) - \kappa \LapN\P[n]
		+ \mI(\P[n]) - \mI'(\P[n])\P[n]
	= 
	\M[n+1]  -  \mI'(\P[n])\P[n+1] -  \frac{2d\kappa}{h^2}\P[n+1]
\end{equation}
\end{subequations}
This is now a linear system of equations, that can be solved directly.
In matrix form, we have
\begin{align}
	\begin{pmatrix} A_{11} & A_{12} \\ A_{21} & A_{22} \end{pmatrix}
	\begin{pmatrix} \P[n+1] \\ \M[n+1] \end{pmatrix}
	&= \begin{pmatrix} b_1 \\ b_2 \end{pmatrix}
\end{align}
with
\begin{subequations}
\begin{align}
	A_{11} &= 1 - \dt \sI_\phi(\P[n], \M[n], t_*) 
\\ 
	A_{12} &= \frac{2d\mob\dt}{h^2} - \dt \sI_\mu(\P[n], \M[n], t_*)
\\ 
	A_{21} &=  -  \mI'(\P[n]) -  \frac{2d\kappa}{h^2}
\\ 
	A_{22} &= 1
\\ 
	b_1 &=	\P[0] + \dt \Bigl[
		\sE(\P[0], \M[0], t_0)
     	- \Nabla(\vect v \P[0])
		+ \mob\LapN\M[n]
    		+ \sI(\P[n], \M[n], t_*)
\notag\\&\qquad
	 	- \sI_\phi(\P[n], \M[n], t_*) \P[n]
		- \sI_\mu(\P[n], \M[n], t_*) \M[n]
	\Bigr]
	\label{eqn:linear_system_b1}
\\ 
	b_2 &= \mE(\P[0]) - \kappa \LapN\P[n]
		+ \mI(\P[n]) - \mI'(\P[n])\P[n]
\end{align}
\end{subequations}
Solving this simple linear system, we can thus evolve $(\P[n], \M[n])$ to $(\P[n+1], \M[n+1])$.
Evaluating the difference between the left hand side and the right hand side of \Eqref{eqn:implicit_equation}, we can determine whether we have converged to a solution.

%For instance, in the simple case of the chemical reaction rate $s(\phi, \mu) = k_{AB} (1 - \phi) - k_{BA} \phi$, we have $s_\phi(\phi,\mu) = - k_{AB}  - k_{BA} =: -$ and $s_\mu(\phi,\mu)=0$.
%We thus define $k_\rm{tot} = k_{AB}  + k_{BA}$ and obtain
%\begin{subequations}
%\begin{align}
%	\P[0]  + \dt k_{AB} + \dt \LapN[\M[n]]
%	&=
%		\P[n+1]  - k_\rm{tot} \dt \P[n+1]  + \frac{2d\dt}{h^2}\M[n+1]
%\\
%	\mu_e(\P[0]) + \mu_c(\P[n]) - \kappa \LapN[\P[n]] 
%	  -  \mu_c'(\P[n])\P[n]
%	&= 
%	\M[n+1]  -  \mu_c'(\P[n])\P[n+1] -  \frac{2d\kappa}{h^2}\P[n+1]
%\end{align}
%\end{subequations}

\subsection{Multi-grid scheme}
The above scheme still converges slowly, but can be enhanced significantly by using a multi-grid scheme.


\section{Energy splitting algorithm for multiple component}
We now derive the energy splitting algorithm for multiple components.
For simplicity, we focus on the case without an advective term, $\vect v=0$, and we treat the chemical reactions term explicitly entirely.
Consequently, we only split the non-linear chemical potential as
\begin{align}
	m_i(\Phi) &= \mE_i(\Phi) + \mI_i(\Phi)
	\;,
\end{align}
where the tilde and hat indicate the respective parts that are solved explicitly and implicitly.
Consequently, the time-discretized \Eqref{eqn:problem} reads
\begin{subequations}
\begin{align}
	\frac{\phi^*_i - \P[0]_i}{\dt}
	&= \sum_j\mob_{ij}\Nabla^2 \mu^*_j  + s_i(\vP[0], \vM[0], t_0)
\\
	\mu^*_i &= \mE_i(\vP[0]) + \mI_i(\Phi^*) - \sum_j\kappa_{ij} \Nabla^2 \phi^*_j
	\;,
\end{align}
\end{subequations}
where we seek $(\Phi^*, \Mu^*)$ at time $t_* = t_0 + \dt$ given $(\vP[0],\vM[0])$ at time $t_0$.
Sorting terms into known and unknown contributions, we have
\begin{subequations}
\begin{align}
	\P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) &= \phi^*_i - \dt\sum_j\mob_{ij}\Nabla^2 \mu^*_j
\\
	\mE_i(\vP[0]) &= \mu^*_i - \mI_i(\Phi^*) + \sum_j\kappa_{ij} \Nabla^2 \phi^*_j
	\;.
\end{align}
\end{subequations}
We need to solve this non-linear system of equations for $(\Phi^*, \Mu^*)$ to advance the system by $\dt$.

\subsection{Iterative solutions of the implicit equations}
The non-linear equation can be solved iteratively, by approximating the solution $(\vP^*, \vM^*)$ in several steps with intermediate values $(\vP[n], \vM[n])$. 
The solution is obtained when the following non-linear equation is obeyed
\begin{subequations}
\label{eqn:implicit_equation_multiple}
\begin{align}
	\P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) &= \P[n]_i - \dt\sum_j\mob_{ij}\Nabla^2 \M[n]_j
\\
	\mE_i(\vP[0]) &= \M[n]_i - \mI_i(\vP[n]) + \sum_j\kappa_{ij} \Nabla^2 \P[n]_j
	\;.
\end{align}
\end{subequations}
Note that the Laplace operator $\Nabla^2$ couples the value at a grid site to its neighbors, which makes solving the above equation difficult.
To make this coupling explicit, we introduce the discretized Laplacian
\begin{align}
	\Nabla^2\psi &\approx \LapN\psi - 2\sum_{k=1}^d \frac{1}{h_k^2} \psi
	\;,
\end{align}
where $\psi$ is a placeholder for $\phi$ and $\mu$.
Here, $\LapN\psi$ contains the sum over the neighbors, which in $d=1$ dimensions reads $\LapN\psi = h_1^{-2}\bigl[\psi(x-h) + \psi(x+h)\bigr]$, where $x$ denotes the spatial position and $h_1$ is the spatial discretization.
Using this notation, the equations read
\begin{subequations}
\label{eqn:cahn_hilliard_timestep_multiple}
\begin{align}
	\P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) &= \P[n]_i - \dt\sum_j\mob_{ij}\left[
		\LapN \M[n]_j - \frac{2d}{h^2} \M[n]_j
	\right]
\\
	\mE_i(\vP[0]) &= \M[n]_i - \mI_i(\vP[n]) + \sum_j\kappa_{ij} \left[
		\LapN \P[n]_j - \frac{2d}{h^2} \P[n]_j
	\right]
	\;,
\end{align}
\end{subequations}
where considered the simple case of equal discretization~$h$ in all dimensions~$d$.
To deal with the spatial coupling, we solve for the values at the approximation step $n+1$ by using the known neighbor values at step~$n$, essentially using the approximation $\LapN\P[n+1]_i \approx \LapN\P[n]_i$.
The values at the iteration $n+1$ should thus obey
%We solve the coupled equations iteratively, by introducing intermediate values $(\P[n], \M[n])$, which are initialized as $\phi^{(0)} = \P[0]$ and $\M[n] = \M[0]$.
%In the limit of large $n$ they should converge to $(\phi^*, \mu^*)$, \ie solve \Eqsref{eqn:cahn_hilliard_timestep_multiple}.
\begin{subequations}
\label{eqn:cahn_hilliard_discretized_2_multiple}
\begin{align}
	\P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) + \dt\sum_j\mob_{ij}\LapN \M[n]_j 
	&= \P[n+1]_i + \frac{2d\dt}{h^2} \sum_j\mob_{ij} \M[n+1]_j
\\
	\mE_i(\vP[0])  - \sum_j\kappa_{ij} \LapN \P[n]_j 
	&= \M[n+1]_i - \mI_i(\vP[n+1]) - \frac{2d}{h^2} \sum_j\kappa_{ij} \P[n+1]_j
	\;.
\end{align}
\end{subequations}
which are now local equations for each site.
We next expand the non-linear term on the right hand side to linear order,
\begin{align}
	  \mI_i(\vP[n+1]) 
	&\approx
	  \mI_i(\vP[n]) + \sum_j \pfrac{\mI_i(\vP[n])}{\phi_j}(\P[n+1]_j - \P[n]_j)
	\;.
\end{align}
We use this in \Eqsref{eqn:cahn_hilliard_discretized_2_multiple} to arrive at
\begin{subequations}
\begin{align}
	\P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) + \dt\sum_j\mob_{ij}\LapN \M[n]_j 
	&= \P[n+1]_i + \frac{2d\dt}{h^2} \sum_j\mob_{ij} \M[n+1]_j
\\
	\mE_i(\vP[0]) 
	+ \mI_i(\vP[n])
	- \sum_j \left[
		\pfrac{\mI_i(\vP[n])}{\phi_j}
		+ \kappa_{ij} \LapN
		\right]\P[n]_j
	&=
	\M[n+1]_i
	- \sum_j \left[
		\pfrac{\mI_i(\vP[n])}{\phi_j}
		+ \frac{2d}{h^2} \kappa_{ij}
	\right]\P[n+1]_j
\end{align}
\end{subequations}
This linear system of equations can be solved directly.
In block-matrix form, we have
\begin{align}
	\begin{pmatrix} \delta_{ij} & A_{ij} \\ B_{ij} & \delta_{ij} \end{pmatrix}
	\begin{pmatrix} \P[n+1]_j \\ \M[n+1]_j \end{pmatrix}
	&= \begin{pmatrix} a_i \\ b_i \end{pmatrix}
\end{align}
with
\begin{subequations}
\begin{align}
	A_{ij} &=  \frac{2d\dt}{h^2} \mob_{ij} 
\\ 
	B_{ij} &=  	- \left[
		\pfrac{\mI_i(\vP[n])}{\phi_j}
		+ \frac{2d}{h^2} \kappa_{ij}
	\right]
\\ 
	a_i &= \P[0]_i  + \dt \, s(\vP[0], \vM[0], t_0) + \dt\sum_j\mob_{ij}\LapN \M[n]_j 
	\label{eqn:linear_system_b1_multiple}
\\ 
	b_i &= 	\mE_i(\vP[0]) 
	+ \mI_i(\vP[n])
	- \sum_j \left[
		\pfrac{\mI_i(\vP[n])}{\phi_j}
		+ \kappa_{ij} \LapN
		\right]\P[n]_j
\end{align}
\end{subequations}
This linear system can be split into two separate linear systems, resulting in the two independent linear equations
\begin{subequations}
\begin{align}
	\left(A_{ik} B_{kj} - \delta_{ij}\right) \P[n]_j &= A_{ik} b_k - a_i
\\
	\left(B_{ik} A_{kj} - \delta_{ij}\right) \M[n]_j &= B_{ik} a_k - b_i
	\;.
\end{align}
\end{subequations}
However, it seems as if solving these two smaller linear systems is actually slower, likely because of some overhead in calling functions.

Solving this linear system, we can thus evolve $(\vP[n], \vM[n])$ to $(\vP[n+1], \vM[n+1])$.
Evaluating the difference between the left hand side and the right hand side of \Eqref{eqn:implicit_equation_multiple}, we can determine whether we have converged to a solution.




\section{Stochastic differential equation for a single component}
The stochastic version of the (extended) Cahn-Hilliard equation reads
\begin{align}
	\Dphi &= \Dt \bigl[\mob\Nabla^2 \mu + s(\phi, \mu) - \Nabla(\vect v \phi) \bigr] + \DW
	\;,
	\label{eqn:sde}
\end{align}
where $\DW$ is a Wiener process describing the noise, which is uncorrelated in time.
This stochastic differential equation is in Ito (?) interpretation.
The variance of the noise is given by $\sigma(\vect r)^2$, which encodes the spatial correlations stemming from the diffusive part of the noise.
In fact, the fluctuations $\xi$ can be decomposed as $\DW = \DW_\mathrm{s} + \Nabla \vect\DW_\mathrm{d}$, where $\DW_\mathrm{s}$ and $\vect\DW_\mathrm{d}$  are the fluctuations of the reactive and diffusive fluxes, respectively.
Because they are derived from linear non-equilibrium thermodynamics, both $\DW_\mathrm{s}$ and $\vect\DW_\mathrm{d}$ are uncorrelated in space.
The final correlations of $\DW$ emerge from the divergence of $\vect\DW_\mathrm{d}$, which also ensures material conservation of the diffusive noise.
Note that we here only consider reaction terms that do not depend on time explicitly, $\partial_t s(\phi, t) = 0$.

\subsection{Explicit algorithm}
The simplest numerical algorithm for solving \Eqref{eqn:sde} is the Euler-Maruyama method, where a simple Euler stepping is used:
\begin{subequations}
\begin{align}
	\P[n+1] &= \P[n] + \dt\left[
		\mob\Nabla^2 \M[n] + s(\P[n], \M[n]) - \Nabla(\vect v \P[n])
	\right] + \W[n]
\\
	\M[n] &= m(\P[n]) - \kappa \Nabla^2 \P[n]
	\;,
\end{align}
\end{subequations}
where $\W[n]$ are randomly distributed normal variables with variance $\dt  \, \sigma^2(\vect r)$.

\subsection{Energy splitting algorithm}
Similarly, the energy splitting algorithm can be extended to noisy simulations by treating the noise explicitly. 
In essence, we need to solve the following linear equations,
\begin{subequations}
\begin{multline}
	\P[0]
	+ \dt\left[
		\sE(\P[0], \M[0])
     	- \Nabla(\vect v \P[0])
		+ \mob \LapN\M[n]
    		+ \sI(\P[n], \M[n])
 		- \sI_\phi(\P[n], \M[n]) \P[n]
		- \sI_\mu(\P[n], \M[n]) \M[n]
	\right]
	+ \W[n]
\\
	=
		\P[n+1]
		+ \frac{2d\mob\dt}{h^2}\M[n+1]
		- \dt \sI_\phi(\P[n], \M[n]) \P[n+1]
		- \dt \sI_\mu(\P[n], \M[n]) \M[n+1]
\end{multline}
and
\begin{equation}
	\mE(\P[0]) - \kappa \LapN\P[n]
		+ \mI(\P[n]) - \mI'(\P[n])\P[n]
	= 
	\M[n+1]  -  \mI'(\P[n])\P[n+1] -  \frac{2d\kappa}{h^2}\P[n+1]
\end{equation}
\end{subequations}
Consequently, we only need to modify \Eqref{eqn:linear_system_b1} to
\begin{align}
	b_1 &=	\P[0] + \W[n] + \dt \Bigl[
		\sE(\P[0], \M[0])
     	- \Nabla(\vect v \P[0])
		+ \mob\LapN\M[n]
    		+ \sI(\P[n], \M[n])
\notag\\&\qquad
	 	- \sI_\phi(\P[n], \M[n]) \P[n]
		- \sI_\mu(\P[n], \M[n]) \M[n]
	\Bigr]
\end{align}
in order to solve the stochastic differential equation using the energy splitting algorithm.


\section{Kramer's diffusion model}
One extension to the model above are concentration-dependent mobilities~$\mob_{ij}$.
In particular, we will consider Kramer's model~\cite{Kramer1984, Mao2018},
\begin{align}
	\mob_{ij}(\vP) = D_{ij} \left(\phi_i\delta_{ij} - \phi_i\phi_j\right)
	\;,
\end{align}
where $D_{ij}$ is the (symmetric) matrix of the diffusivities.
Note that in the simple case where $D_{ij}=D$ and the chemical potentials are derived from the entropic terms alone (ideal gas), this choice of mobilities reduces the diffusive fluxes driven by chemical potentials to Fick's law, $\partial_t \phi_i = D\nabla^2 \phi_i$.

The evolution equations (without advection or noise) read
\begin{align}
	\partial_t \phi_i &= \sum_j \Nabla\left[
		D_{ij} \left(\phi_i\delta_{ij} - \phi_i\phi_j\right) \Nabla \mu_j
	\right] + s_i(\Phi, \Mu, t) 
\end{align}
where $\Mu$ is given by \Eqref{eqn:problem_mu}.
Expanding the derivatives, we obtain
\begin{align}
	\partial_t \phi_i &= \sum_j 
		D_{ij} \left[(\delta_{ij} - \phi_j)\Nabla\phi_i - \phi_i\Nabla \phi_j \right] . \Nabla \mu_j
	+ \sum_j 
		D_{ij} \left(\phi_i\delta_{ij} - \phi_i\phi_j\right) \Nabla^2 \mu_j
	+ s_i(\Phi, \Mu, t) 
\end{align}
which can be used directly in the explicit version of the algorithm.

\bibliographystyle{plain}
\bibliography{../bibliography}

\end{document}
