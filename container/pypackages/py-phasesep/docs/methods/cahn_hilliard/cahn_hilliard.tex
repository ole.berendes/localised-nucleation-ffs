\documentclass[a4paper, 11pt, DIV13, numbers=noenddot]{scrartcl}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{xspace}
\usepackage{xifthen}


\newcommand{\Eqref}[1]{\mbox{Eq.\hspace{0.25em}\eqref{#1}}}
\newcommand{\Eqsref}[1]{\mbox{Eqs.\hspace{0.25em}\eqref{#1}}}
\newcommand{\set}[1]{\{#1\}}
\newcommand{\mean}[1]{\langle#1\rangle}

\newcommand{\ie}{\mbox{i.\hspace{0.125em}e.}\@\xspace}
\newcommand{\vect}{\boldsymbol}
\newcommand{\Nabla}{\vect\nabla}
\newcommand{\kBT}{k_\mathrm{B} T}

\newcommand{\diff}{\mathrm{d}}
\newcommand{\Difffrac}[2]{\frac{\mathrm{D} #1}{\mathrm{D} #2}}
\newcommand{\dt}{\Delta t}
\newcommand{\Dt}{\diff t}
\newcommand{\DW}{\diff W}
\newcommand{\Dphi}{\diff \phi}
\newcommand{\mob}{\Lambda}

\numberwithin{equation}{section}

\begin{document}

\title{Multi-component Cahn-Hilliard equation}
\author{David Zwicker \and Jan Kirschbaum \and Noah Ziethen}

\maketitle
\tableofcontents

\section{General introduction}
The aim of this document is to derive the Cahn-Hilliard equation describing the phase separation kinetics of a mixture with $N$ components and particularly introduce incompressibility correctly.
We describe the system on the level of classical field theory, where each species~$i$ is described by its number density field~$c_i(\vect r, t)$.
The overall fluid has a mass density $\rho(\vect r, t)$, which is given by
\begin{align}
	\rho = \sum_i m_i c_i	
	\label{eqn:mass_density}
	\;,
\end{align}
where we for simplicity only consider equal molecular masses, $m_i=m$.
We can now choose to describe the system bei either (i) specifying all concentrations~$c_i$, (ii) specifying $\rho$ and $N-1$ of the concentration fields, or (iii) specifying $\rho$, $N$ concentration fields, and the constraint \eqref{eqn:mass_density}.
It turns out that including~$\rho$ in the description is useful since it allows to separate the center-of-mass motion from relative fluxes.
This is particularly useful for incompressible systems, where $\rho=\mathrm{const.}$ is an additional constraint; see Section~\ref{sec:incompressible}.

%Using these definitions, we can introduce a frame that moves with the center-of-mass velocity, implying
%\begin{align}
%	\rho \Difffrac{v_{\alpha}}{t} - \partial_{\beta} \sigma^\mathrm{CM}_{\alpha\beta} &= f^\mathrm{ext}_{\alpha}
%&
%	\sigma^\mathrm{CM}_{\alpha\beta} &= \sigma^\mathrm{tot}_{\alpha\beta} +\rho v_{\alpha} v_{\beta}
%\end{align}

\section{Compressible fluid}
\label{sec:compressible}

We first consider a compressible system, where $\rho$ can in principle vary.
Since we only consider fluids, the entire space must be filled, implying that the volume fractions~$\phi_i = \nu_i c_i$ obey $\sum_i \phi_i = 1$.
A compressible system thus implies that the molecular volumes~$\nu_i$ can vary.

\subsection{Thermodynamics}
For simplicity, we here consider the case where all species have identical molecular volume, $\nu_i = \nu$.
%We describe the state a the mixture of $N$ components with constant molecular volumes~$\nu$ by their volume fractions $\phi_i(\vect r, t)$, where we choose $i=0,\ldots,N-1$ to comply with the indexing of \texttt{python}.
We consider an isothermal system in a container of fixed volume whose free energy reads
\begin{align}
	\label{eqn:free_energy}
	 \frac{F}{\kBT} &=  \int \Biggl[ 
		\frac{\rho v_\alpha^2}{2}
%\notag\\&\qquad
		+ f_0
		+\frac{K}{2}\biggl(\frac1\nu - \sum_{i=1}^{N} c_i\biggr)^{\!\!2}
	\Biggr] \diff^3 r
		\;,
\end{align}
where the first term captures the kinetic energy of the center-of-mass motion,
the second term captures the interactions of the different particles using a free energy density~$f_0$,
and the third term the compressibility of the entire mixture with a bulk modulus~$K$.
A typical choice for the free energy density is the Flory-Huggins free energy,
\begin{align}
	f_0&= \sum_{i=1}^{N} \biggl(
			c_i \log (\nu c_i)
        			+ w_i  c_i
        			+\frac{\nu}{2} \sum_{j=1}^{N} \left[
        				\chi_{ij} c_i  c_j
        				- \kappa_{ij}\partial_\alpha c_i \partial_\alpha c_j
        			\right]
		\biggr)
	\;,
\end{align}
where the first term captures entropic contributions, the second term the internal energy, the third term interactions between different species, and the fourth term accounts for gradient penalties.
Here, the Flory-Parameter matrix obeys $\chi_{ij} = \chi_{ji}$ and $\chi_{ii} = 0$.
Moreover, the gradient parameters are typically chosen as $\kappa_{ij} = \lambda^2 \chi_{ij}$, where $\lambda$ is a molecular length scale~\cite{Mao2018}.
For this choice, the interaction matrix must also fulfill $\sum_{i,j} a_i \chi_{ij} a_j < 0$ for any $a_i$ with $\sum_i a_i =0$ for the interface to be stable~\cite{Mao2018}.

Using equilibrium thermodynamics, we can then define the pressure and the chemical potentials $\mu_i = \delta  F/\delta  c_i$ associated with the free energy~$ F$,
%We will only require the chemical potential, which reads
\begin{align}
	\mu_i = \kBT\left[
            	\log (\nu c_i) +  w_i
            	+ \nu\sum_{j=1}^{N} \chi_{ij}  c_j 
            	+ \nu\sum_{j=1}^{N} \kappa_{ij} \partial_\alpha^2 c_j
            	- K \biggl(\frac1\nu - \sum_{j=1}^{N}  c_j \biggr)
	\right]
	\label{eqn:chemical_potential}
	\;.
\end{align}
%and drive the actual kinetics.
Another crucial quantity is the hydrostatic pressure~$P$, which is given by~\cite{Julicher2018,Weber2019}
\begin{align}
	P = \sum_{i=1}^{N}  c_i \mu_i - f
\end{align}
where $f$ is the integrand of \Eqref{eqn:free_energy}.
Note that this implies
\begin{align}
	\partial_\alpha P &= \sum_{i=1}^{N} \bigl(
			\mu_i \partial_\alpha  c_i + 
			 c_i \partial_\alpha \mu_i
		\bigr)
		 - \partial_\alpha f
%\notag\\ \partial_\alpha P &= \sum_{i=1}^{N} \bigl(
%			\mu_i \partial_\alpha  c_i + 
%			 c_i \partial_\alpha \mu_i
%		\bigr)
%		 - \sum_{i=1}^{N} \frac{\delta  F}{\delta  c_i}\partial_\alpha  c_i
%\notag\\ &
	= \sum_{i=1}^{N}  c_i \partial_\alpha \mu_i
\;,
\end{align}
which is a Gibbs-Duhem relation; see \cite[Eq. 46]{Julicher2018}.

\subsection{Kinetics}
To derive the kinetics of the system, it is useful to start with the conservations laws and then expand around the equilibrium state~\cite{Julicher2018}.

\subsubsection{Conservation laws}

This mass density must obey momentum conservation,
\begin{align}
	\partial_t \rho + \partial_\alpha (v_\alpha \rho) = 0
	\label{eqn:momentum_conservation}
	\;,
\end{align}
where $v_\alpha$ is the center-of-mass velocity of the fluid.
Newton's law links changes in the momentum density $v_\alpha \rho$ to the total stress,
\begin{align}
	\partial_t (v_\alpha \rho) = \partial_\beta \sigma^\mathrm{tot}_{\alpha\beta}
	\;,
\end{align}
which can be split into three parts,
\begin{align}
	\sigma^\mathrm{tot}_{\alpha\beta} &= \tilde\sigma^\mathrm{tot}_{\alpha\beta} + \sigma^\mathrm{tot, a}_{\alpha\beta} - P\delta_{\alpha\beta}
	\;,
\end{align}
denoting shear stress, anti-symmetric stress, and isotropic stress, respectively.
Consequently, $P=-\sigma^\mathrm{tot}_{\alpha\alpha}$ is the pressure and $\tilde\sigma^\mathrm{tot}_{\alpha\beta} = \frac{1}{2}(\sigma^\mathrm{tot}_{\alpha\beta} + \sigma^\mathrm{tot}_{\beta\alpha})$ is the symmetric part of the stress.
Note that we assume that the antisymmetric part, $\sigma^\mathrm{tot, a}_{\alpha\beta} = \frac{1}{2}(\sigma^\mathrm{tot}_{\alpha\beta} - \sigma^\mathrm{tot}_{\beta\alpha})$, vanishes for simplicity.

In the simple case without chemical reactions, the system also conserves the individual particles, implying the continuity equation
\begin{align}
	\partial_t c_i + \partial_\alpha J^i_\alpha = 0
\end{align}
where $J^i_\alpha$ are particle fluxes in the lab frame.
The center-of-mass velocity can be expressed as
\begin{align}
	v_\alpha = \frac{\sum_i J^i_\alpha}{\sum_i c_i}
	\;,
\end{align}
so we can also define relative particle fluxes $j^i_\alpha = J^i_\alpha -  c_i v_\alpha$, which obey
\begin{align}
	\sum_i j^i_\alpha = 0
	\;.
	\label{eqn:sum_of_relative_fluxes}
\end{align}
We can then write the dynamics of the density field as
\begin{align}
	\partial_t c_i + \partial_\alpha(v_\alpha c_i) = - \partial_\alpha j^i_\alpha
	\;.
	\label{eqn:continuity_relative}
\end{align}

%In a conserved system (without reactions), the volume fractions change due to spatial fluxes,
%\begin{align}
%	\partial_t c_i + \partial_\alpha( c_i v_\alpha + j^i_\alpha) = 0
%	\;.
%\end{align}


\subsubsection{Linear non-equilibrium thermodynamics}
The center-of-mass velocity~$v_\alpha$ obeys the Navier-Stokes equation~\cite{Julicher2018},
\begin{align}
	 \partial_t (\rho v_\alpha) &=  - \partial_\beta (\rho v_\alpha v_\beta) - \partial_\alpha P
	 \;.
\end{align}
Combining this with \Eqref{eqn:momentum_conservation}, we obtain the usual form
\begin{align}
%	\rho  \partial_t v_\alpha 
%	-v_\alpha \partial_\beta( \rho v_\beta) 
%	- m v_\alpha \sum_i \partial_\beta j^i_\beta
%	 &=  - \partial_\beta (\rho v_\alpha v_\beta) - \partial_\alpha P
	\partial_t v_\alpha 
	+ v_\beta \partial_\beta v_\alpha
	 &=  
	 - \frac{\partial_\alpha P}{\rho}
	\;.
\end{align}
In contrast, the relative fluxes are driven by gradients in chemical potentials.
However, if we naively use the chemical potentials given in \Eqref{eqn:chemical_potential}, the resulting fluxes would violate the constraint~\eqref{eqn:mass_density}.
In essence, this is because we would have treated $N+1$ fields ($\rho$, $c_i$) as fully independent.
To fix this, we enforce the constraint~\eqref{eqn:mass_density} using a Lagrange multiplier~$\zeta(\vect r)$ by introducing the modified free energy
\begin{align}
	\tilde F = F + \int \zeta \biggl(\frac{\rho}{m} - \sum_{i=1}^{N} c_i\biggr) \diff^3r
	\;.
\end{align}
This leads to modified chemical potentials and pressures
\begin{align}
	\tilde \mu &= \mu - \zeta
&
	\tilde P &= P  - \zeta \frac{\rho}{m} 
	\;.
\end{align}
In particular, gradients of the chemical potential drive the relative fluxes~\cite{Julicher2018}
\begin{align}
	j^i_\alpha = -\sum_{j=1}^N \Lambda_{ij} (\partial_\alpha \mu_j - \partial_\alpha \zeta)
	\label{eqn:comp_linear_fluxes}
	\;,
\end{align}
where the mobilities $\Lambda_{ij}$ form a symmetric and positive semidefinite Onsager matrix, which can depend on composition.
Here, the Lagrange multiplier~$\zeta$ must be chosen such that the condition \Eqref{eqn:sum_of_relative_fluxes} is fulfilled~\cite[Sec. 3.1]{Ibbeken2020}.
Plugging~\Eqref{eqn:comp_linear_fluxes} into \Eqref{eqn:sum_of_relative_fluxes}, we find
\begin{align}
	\partial_\alpha\zeta &= -\frac{\sum_{k,l} \Lambda_{kl} \partial_\alpha \mu_l }{\sum_{k,l} \Lambda_{kl}}
	\;.
\end{align}
Hence,
\begin{align}
	j^i_\alpha 
	&= \sum_{j=1}^N \Lambda_{ij} \left(
		\frac{\sum_{k,l} \Lambda_{kl} \partial_\alpha \mu_l }{\sum_{k,l} \Lambda_{kl}}
		-\partial_\alpha \mu_j 
	\right)
\notag\\&=  \frac{1}{\sum_{k,l} \Lambda_{kl}}\sum_{j,k,l=1}^N \Lambda_{ij}\Lambda_{kl} \left(
		\partial_\alpha \mu_l
		- \partial_\alpha \mu_j  
	\right)
	\;.
\end{align}
This can again be expressed as a linear map,
\begin{align}
	j^i_\alpha &= - \sum_{j=1}^{N} \bar\Lambda_{ij}\partial_\alpha \mu_j
	\;,
\end{align}
with the modified mobility matrix
\begin{align}
	\bar\Lambda_{ij} = 
%	\frac{\sum_{k,l} \left( \Lambda_{ij}\Lambda_{kl}  - \Lambda_{il}\Lambda_{kj}\right)}
%	{\sum_{k,l} \Lambda_{kl}}
	\Lambda_{ij} -  \frac{\sum_{k,l} \Lambda_{il}\Lambda_{kj}}{\sum_{k,l} \Lambda_{kl}}
	\;,
\end{align}
which obeys 
\begin{align}
	\sum_{i=1}^N \bar\Lambda_{ij} &= 0
	\label{eqn:mobility_sum_property}
	\;,
\end{align}
i.e., the row and column sums vanish.
This property also implies 	$\bar\Lambda_{Nj} = - \sum_{i=1}^{N-1} \bar\Lambda_{ij}$, which will become crucial when discussing the incompressible case below.

Taken together, we showed that the relative fluxes $j^i_\alpha$ need to be expressed for only $N-1$ components, because the flux of the other component can always be reconstructed.
We thus have
\begin{align}
	j^i_\alpha &= - \sum_{j=1}^{N-1} \bar\Lambda_{ij}\partial_\alpha \mu_j
	\label{eqn:comp_relative_fluxes}
\end{align}
for $i=1,\ldots,N-1$.
Linear non-equilibrium thermodynamics does not tell us directly what a proper choice of $\bar\Lambda_{ij}$ would be, but since the result should be independent of which component we chose to ignore, we need to find a symmetric solution.
%In particular, we would like to use information from microscopic dynamics to inform a useful choice, e.g.,  the fact that mobilities typically scale with the concentration.
While simply ignoring the $N$-th row and column treats one species differently than the others, we showed above that treating condition~\Eqref{eqn:mass_density} using a Lagrange multiplier leads to the consistent choice
\begin{align}
	\bar\Lambda_{ij} = \Lambda_{ij} -  \frac{\sum_{k,l} \Lambda_{il}\Lambda_{kj}}{\sum_{k,l} \Lambda_{kl}}
	\label{eqn:mobility_modified}
	\;,
\end{align}
which obeys \Eqref{eqn:mobility_sum_property}.
Moreover, the expression reduces to Kramer's model~\cite{Mao2018} for the basic choice $\Lambda_{ij}=\lambda_0 c_i \delta_{ij}$, which we will show in the next section.


\subsubsection{Special mobility matrices}
For a diagonal mobility matrix, $\Lambda_{ij} = \Lambda_i \delta_{ij}$, we find
\begin{align}
	\bar\Lambda^\mathrm{diag}_{ij} &= 
%	\frac{\sum_{k,l=1}^N \Lambda_i\Lambda_k \delta_{ij}\delta_{kl} 
%	- \sum_{k,l=1}^N  \Lambda_{il}\Lambda_{kj} \delta_{il}\delta_{kj}}
%	{\sum_{k=1}^N \Lambda_k}
%\notag\\&=
%	\frac{\sum_{k=1}^N \Lambda_i\Lambda_k \delta_{ij} -  \Lambda_i\Lambda_j}
%	{\sum_{k=1}^N \Lambda_k}
%\notag\\&=
	\Lambda_i \delta_{ij} -  \frac{\Lambda_i\Lambda_j} {\sum_k \Lambda_k}
	\;.
\end{align}
This demonstrates that off-diagonal terms originate even for the simplest choice of constant, diagonal mobilities, $\Lambda_{ij} = \Lambda_0 \delta_{ij}$, resulting in
\begin{align}
	\bar\Lambda^\mathrm{const}_{ij} &= 
		\Lambda_0 \delta_{ij} -  \frac{\Lambda_0} {N}
	\;.
\end{align}
Another interesting case is the choice $\Lambda_{ij} = \lambda_i c_i \delta_{ij}$~\cite{deGennes1980,Ibbeken2020}, which is related to Kramer's model~\cite{Mao2018},
\begin{align}
	\bar\Lambda^\mathrm{kramer}_{ij} &= 
		\lambda_i c_i \delta_{ij} -  \frac{\lambda_i \lambda_j c_i c_j} {\sum_{k=1}^N \lambda_k c_k}
	\;.
\end{align}
If all components have equal kinetics, $\lambda_i=\lambda_0$, we find
\begin{align}
	\bar\Lambda^\mathrm{kramer,0}_{ij} &= 
		\lambda_0 \left[ c_i \delta_{ij} -  \frac{c_i c_j} {\sum_{k=1}^N c_k} \right]
	\label{eqn:comp_mobility_kramer0_xi}
	\;.
\end{align}


\subsection{Stochastic dynamics}
We next add fluctuations~$\xi^i_\alpha$ to the diffusive fluxes~$j^i_\alpha$,
\begin{align}
	j^i_\alpha &= - \sum_{j=1}^N\Lambda_{ij} (\partial_\alpha \mu_j - \partial_\alpha \zeta)
		+ \xi^i_\alpha
	\;.
\end{align}
The structure of the noise follows from linear non-equilibrium thermodynamics~\cite{Julicher2018},
\begin{align}
	\mean{\xi^i_\alpha(t)} &= 0
&
	\mean{\xi^i_\alpha(t) \xi^j_\beta(t')} &= 2 \kBT \Lambda_{ij}\delta_{\alpha\beta}\delta(t-t')
	\;,
	\label{eqn:comp_stochastic_basic}
\end{align}
i.e., the noise has zero mean, is uncorrelated in time as well as direction, and the covariance is proportional to the Onsager matrix~$\Lambda_{ij}$ to obey the fluctuation-dissipation theorem.
The fluctuations~$\xi^i_\alpha$ need to obey the constraint~\eqref{eqn:sum_of_relative_fluxes}, which we ensure using the modified mobility matrix given in \Eqref{eqn:mobility_modified},
\begin{align}
	\xi^N_\alpha &= 1 - \sum_{i=1}^{N-1} \xi^i_\alpha
&
	\mean{\xi^i_\alpha(t) \xi^j_\beta(t')} &= 2 \kBT \bar\Lambda_{ij}\delta_{\alpha\beta}\delta(t-t')
\end{align}
for $i, j=1,\ldots, N-1$.
This automatically ensures the correct correlations between the fluctuations of the $N$-th component and all others,
\begin{align}
	\mean{\xi^i_\alpha(t) \xi^N_\beta(t')}
	&= - \sum_{j=1}^{N-1} \mean{\xi^i_\alpha(t) \xi^j_\beta(t')}
%\notag\\
	= - 2 \kBT \delta_{\alpha\beta}\delta(t-t') \sum_{j=1}^{N-1} \bar\Lambda_{ij}
%\notag\\
	= 2 \kBT \delta_{\alpha\beta}\delta(t-t') \bar\Lambda_{iN}
	\;.
\end{align}
The correlations in the noise can be implemented conveniently using uncorrelated noises~$\eta^i_\alpha$,
\begin{align}
	\mean{\eta^i_\alpha(t)} &= 0
&
	\mean{\eta^i_\alpha(t) \eta^j_\beta(t')} &= \delta_{ij}\delta_{\alpha\beta}\delta(t-t')
	\;,
\end{align}
and a Cholesky decomposition~$O_{ij}$ of the Onsager matrix,
\begin{align}
	\xi^i_\alpha &= \sum_{j=1}^{N-1} O_{ij} \eta^j_\alpha
& \text{with} &&
	\sum_{k=1}^{N-1} O_{ik}O_{jk} &= 2\kBT \bar\Lambda_{ij}
	\;.
\end{align}
For simple cases, this decomposition can be performed directly. 
For instance, if $\Lambda_{ij} = \Lambda_0 \delta_{ij}$, we have $\bar\Lambda_{ij} = \Lambda_0 \delta_{ij} -  \frac{\Lambda_0} {N}$, which can be ensured by
\begin{align}
	O_{ij} &= \sqrt{2\kBT \Lambda_0}\left[ \delta_{ij} - \frac{1}{N \pm \sqrt{N}}\right]
	\;.
\end{align}
%\begin{align}
%	\sum_{k=1}^{N-1} O_{ik}O_{jk} &= 
%	\sum_{k=1}^{N-1} \sqrt{2\kBT \Lambda_0}\left[ \delta_{ik} -  x\right]\sqrt{2\kBT \Lambda_0}\left[ \delta_{jk} -  x\right]
%\\
%	&= 2\kBT\Lambda_0
%	\sum_{k=1}^{N-1} \left[ \delta_{ik} -  x\right]\left[ \delta_{jk} -  x\right]
%\\
%	&= 2\kBT\Lambda_0
%	\sum_{k=1}^{N-1} \left[ \delta_{ik}\delta_{jk} - \delta_{jk}x -   \delta_{ik}x + x^2\right]
%\\
%	&= 2\kBT\Lambda_0
%	 \left[ \delta_{ij} -  x -  x +  (N-1)x^2\right]
%\end{align}
We have not yet found a way to conveniently express the decomposition for Kramer's model, which would amount to find a matrix $O$ such that
\begin{align}
	\sum_{k=1}^{N-1} O_{ik}O_{jk} &= 2\kBT \lambda_0 \bigl[\phi_i\delta_{ij} - \phi_i \phi_j\bigr]
\end{align}
%If we use
%\begin{align}
%	O_{ij} = \sqrt{2\kBT \lambda_0} \left[\delta_{ij} \sqrt{\phi_i} + Q_{ij}\right]
%\end{align}
%we have 
%\begin{align}
%	\sum_{k=1}^{N-1} O_{ik}O_{jk} 
%	&= 2\kBT \lambda_0
%	\sum_{k=1}^{N-1}\left[\delta_{ik} \sqrt{\phi_i} + Q_{ik}\right]\left[\delta_{jk} \sqrt{\phi_j} + Q_{jk}\right]
%\\
%	&= 2\kBT \lambda_0\left[
%		\delta_{ij} \phi_i
%		+  \sqrt{\phi_i} Q_{ji}
%		+ \sqrt{\phi_j} Q_{ij} 
%		+ \sum_{k=1}^{N-1}Q_{ik}Q_{jk}
%	\right]
%\end{align}

\section{Incompressible fluid}
\label{sec:incompressible}
The simpler case of an incompressible fluid is obtained in the limit of a large bulk modulus~$K$.
The large pressures implied by this choice impose the constraint $\sum_i c_i = \nu^{-1}$.
In this case, it is convenient to introduce volume fractions $\phi_i = \nu c_i$, where it is sufficient to specifiy the first $N-1$ components.
We thus remove the last component, which is given by
\begin{align}
	\phi_N = 1 - \sum_{i=1}^{N-1} \phi_i
	\;,
\end{align}
and implies $\sum_{i=1}^N \partial_\alpha \phi_i = 0$.
These substitutions reduce the problem dimension and can accelerate simulations.

In the considered limit of equal molecular volumes and densities, we also immediately find that the mass density~$\rho$ is constant everywhere and the center-of-mass velocity $v_\alpha$ must be divergence-free, $\partial_\alpha v_\alpha=0$.
This condition is ensured by the pressure field, which can be arbitrarily large because of the large modulus~$K$.
Mathematically, it is convenient to consider the pressure~$P$ as a Lagrange multiplier that ensures the condition $\partial_\alpha v_\alpha=0$.

\subsection{Thermodynamics}
To derive the equations describing the simpler problem, we introduce the new total free energy
\begin{align}
	\label{eqn:free_energy_Neduced}
	\bar F = \frac{\kBT}{\nu} \int \biggl[ 
		\sum_{i=1}^{N-1} \phi_i \log\phi_i 
		+ \phi_N \log \phi_N
		+ \sum_{i=1}^{N-1} \bar w_i \phi_i
		+ \sum_{i,j=1}^{N-1} \frac{\bar\chi_{ij}}{2} \phi_i \phi_j
		+ \sum_{i,j=1}^{N-1} \frac{\bar\kappa_{ij}}{2} \partial_\alpha\phi_i \partial_\alpha\phi_j
	\biggr] \diff^3 r
	\;,
\end{align}
In deriving this expression, we neglected a constant offset (which does not affect the physics) and defined
\begin{subequations}
\begin{align}
	\bar w_i &=  w_i -  w_N + \frac{\chi_{iN} + \chi_{Ni}}{2}
\\
	\bar \chi_{ij} &= \chi_{ij} - \chi_{iN} - \chi_{Ni}
\\
  \bar \kappa_{ij} &= \kappa_{iN} + \kappa_{Nj} - \kappa_{ij} - \kappa_{NN}
\end{align}
\end{subequations}
%Note that the $\kappa$ defined here is actually positive
%Note that the values $\chi_{ij}$, and thus also $\kappa_{ij}$, are often negative.
Note that the term proportional to $\bar\kappa$ in \Eqref{eqn:free_energy_Neduced} carries a positive sign, while the corresponding term in the original equation \Eqref{eqn:free_energy}  has a negative sign.
If we scale $\bar\kappa$ according to the interaction matrix, we obtain $\bar\kappa_{ij} =-\lambda^2\bar\chi_{ij}= \lambda^2\left[\chi_{iN} + \chi_{Nj} - \chi_{ij} \right]$.
The associated chemical potentials, defined as $\bar\mu_i = \nu\delta \bar F/\delta \phi_i$, are then exchange chemical potentials, $\bar\mu_i = \mu_i - \mu_N$, and read
\begin{subequations}
\begin{align}
	\bar\mu_i &= \kBT \biggl(
		\log\phi_i  - \log \phi_N
		+ \bar w_i
		+ \sum_{j=1}^{N-1} \bar\chi_{ij} \phi_j
		- \sum_{j=1}^{N-1} \bar\kappa_{ij} \partial_\alpha^2\phi_j
	\biggr)
\\
&=  \kBT \biggl(
		\log\phi_i  - \log \phi_N
		+ \bar w_i
		+ \sum_{j=1}^{N-1} \bar\chi_{ij}\left(1 -  \lambda^2\partial_\alpha^2\right) \phi_j
	\biggr)
	\;.
\end{align}
\end{subequations}
The chemical potentials are exchange chemical potentials describing the free energy change when species $i$ is replaced by species $N$~\cite{Weber2019}.

\subsection{Kinetics}
We consider the simple case where bulk flows are absent, $v_\alpha=0$, and focus on the relative fluxes.
From the expression \Eqref{eqn:comp_relative_fluxes} for the compressible case, we immediately obtain
\begin{align}
	j^i_\alpha &= - \sum_{j=1}^{N-1} \bar\Lambda_{ij}\partial_\alpha \bar\mu_j
	\;,
\end{align}
for $i=1,\ldots,N-1$.
Note that we replaced the chemical potential in \Eqref{eqn:comp_relative_fluxes} by exchange chemical potentials, which does not affect the result, since the contributions of $\mu_N$ vanish because of \Eqref{eqn:mobility_sum_property}.

In the special case of Kramer's model, $\Lambda_{ij} = \lambda_i \phi_i \delta_{ij}$, we have
\begin{align}
	\bar\Lambda_{ij}  = \lambda_i \phi_i \delta_{ij} -  \frac{\lambda_i \lambda_j \phi_i \phi_j} {\sum_k \lambda_k \phi_k}
	\;.
\end{align}
%This further simplifies if $\lambda_i = \lambda$,
%\begin{align}
%	\bar\Lambda_{ij}  = \lambda \left[\phi_i \delta_{ij} - \phi_i \phi_j\right]
%\end{align} 
Defining $\hat\phi_i = \lambda_i\phi_i$ and $\Phi=\sum_k \hat\phi_k$, we thus have
$\bar\Lambda_{ij}  = \hat\phi_i \delta_{ij} -  \hat\phi_i \hat\phi_j  \Phi^{-1}$
and the associated Cahn-Hilliard equation can be expressed as
\begin{align}
	\partial_t \phi_i &= 
%	\partial_\alpha\Biggl(
%		\sum_{j=1}^{N-1} \bar\Lambda_{ij}\partial_\alpha \bar\mu_j
%	\Biggr)
%\notag\\&= 
	\sum_{j=1}^{N-1} (\partial_\alpha \bar\Lambda_{ij})(\partial_\alpha \bar\mu_j)
	+ \sum_{j=1}^{N-1} \bar\Lambda_{ij}\partial_\alpha^2 \bar\mu_j
%\notag\\&= \sum_j \partial_\alpha\biggl(
%		\hat\phi_i \delta_{ij} -  \frac{\hat\phi_i \hat\phi_j}{\Phi}
%	\biggr)\partial_\alpha \bar\mu_i
%	+ \sum_j \biggl(
%		\hat\phi_i \delta_{ij} -  \frac{\hat\phi_i \hat\phi_j}{\Phi}
%	\biggr)\partial_\alpha^2 \bar\mu_i
%\notag\\&= 
%	(\partial_\alpha\hat\phi_i)(\partial_\alpha \bar\mu_i)
%	- \sum_j \frac{\hat\phi_i \partial_\alpha\hat\phi_j}{\Phi} \partial_\alpha \bar\mu_j
%	- \sum_j \frac{\hat\phi_j \partial_\alpha\hat\phi_i}{\Phi} \partial_\alpha \bar\mu_j
%	+ \sum_j \frac{\hat\phi_j \hat\phi_i}{\Phi^2} (\partial_\alpha\Phi) (\partial_\alpha \bar\mu_j)
%	+ \sum_j \biggl(
%		\hat\phi_i \delta_{ij} -  \frac{\hat\phi_i \hat\phi_j}{\Phi}
%	\biggr)\partial_\alpha^2 \bar\mu_i
\notag\\&= 
	(\partial_\alpha\hat\phi_i)(\partial_\alpha \bar\mu_i)
	- \sum_j \left[
		\frac{\hat\phi_i \partial_\alpha\hat\phi_j}{\Phi}
		+ \frac{\hat\phi_j \partial_\alpha\hat\phi_i}{\Phi}
		- \frac{\hat\phi_j \hat\phi_i \partial_\alpha\Phi}{\Phi^2}
	\right] \partial_\alpha \bar\mu_j
	+ \sum_j \biggl(
		\hat\phi_i \delta_{ij} -  \frac{\hat\phi_i \hat\phi_j}{\Phi}
	\biggr)\partial_\alpha^2 \bar\mu_i
	\;.
\end{align}
%\begin{align}
%	\partial_t \phi_i &= \partial_\alpha\Biggl(
%		\sum_{j=1}^{N-1} \bar\Lambda_{ij}\partial_\alpha \bar\mu_j
%	\Biggr)
%%\notag\\&= \partial_\alpha\Biggl(
%%		\lambda_i \phi_i \partial_\alpha \bar\mu_i
%%		- \lambda_i \phi_i  \frac{ \sum_j\lambda_j \phi_j \partial_\alpha \bar\mu_j}
%%			{\sum_k \lambda_k \phi_k}
%%	\Biggr)
%\notag\\&= \partial_\alpha\biggl(
%		\hat\phi_i 
%		- A \hat\phi_i  \sum_j\hat\phi_j
%	\biggr)\partial_\alpha \bar\mu_i
%	+ \biggl(
%		\hat\phi_i 
%		- A \hat\phi_i  \sum_j\hat\phi_j \partial_\alpha \bar\mu_j
%	\biggr)\partial_\alpha^2 \bar\mu_i
%\notag\\&= \partial_\alpha\biggl(
%		\hat\phi_i \partial_\alpha \bar\mu_i
%		- A \hat\phi_i  \sum_j\hat\phi_j \partial_\alpha \bar\mu_j
%	\biggr)
%\notag\\&= 
%	 \hat\phi_i \partial_\alpha^2 \bar\mu_i
%	+  (\partial_\alpha \hat\phi_i) (\partial_\alpha \bar\mu_i)
%	-  A\sum_j\hat\phi_j (\partial_\alpha \bar\mu_j)(\partial_\alpha  \hat\phi_i )
%	- \hat\phi_i \partial_\alpha A \sum_j\hat\phi_j \partial_\alpha \bar\mu_j
%%\notag\\&= \lambda_i\left[
%%	 \phi_i \partial_\alpha^2 \bar\mu_i  % term 1
%%	+  (\partial_\alpha \phi_i) (\partial_\alpha \bar\mu_i)  % term 2
%%	-  \frac{\sum_j\lambda_j \phi_j \partial_\alpha \bar\mu_j}{sum} \partial_\alpha  \phi_i   % term 3
%%	- \phi_i  \frac{ \sum_j\lambda_j \phi_j \partial_\alpha^2 \bar\mu_j}{\sum_k \lambda_k \phi_k}  % term 4
%%	- \phi_i \frac{ \sum_j\lambda_j (\partial_\alpha \phi_j)(\partial_\alpha \bar\mu_j)}{\sum_k \lambda_k \phi_k}  % term 5
%%	+ \phi_i  \frac{ \sum_j\lambda_j \phi_j \partial_\alpha \bar\mu_j}{sum^2} \sum_k\lambda_k\partial_\alpha \phi_k  % term 6
%%\right]
%\notag\\&= \hat\phi_i \sum_j\left[
%		\delta_{ij} % term 1 
%		- A \hat\phi_j  % term 4
%	\right]\partial_\alpha^2 \bar\mu_j
%%\notag\\&\quad
%	+ \sum_j \left[
%		\delta_{ij} \partial_\alpha \hat\phi_j % term 2
%		-  A\hat\phi_j (\partial_\alpha \hat\phi_i)% term 3 
%		-  A \hat\phi_i (\partial_\alpha \hat\phi_j)% term 5
%	\right](\partial_\alpha \bar\mu_j)
%\notag\\&\quad
%	+ \hat\phi_i  A^2 \sum_{j,k} \hat\phi_j (\partial_\alpha \hat\phi_k)(\partial_\alpha \bar\mu_j)  % term 6
%\notag\\&= \sum_j\left[
%		\delta_{ij} % term 1 
%		- A \hat\phi_j  % term 4
%	\right]\left[
%		\hat\phi_i \partial_\alpha^2 \bar\mu_j 
%		+ (\partial_\alpha \hat\phi_i)(\partial_\alpha \bar\mu_j)
%	\right]
%%\notag\\&\quad
%	- A \hat\phi_i \sum_j \left[
%		\partial_\alpha \hat\phi_j
%		- A \hat\phi_j \sum_k \partial_\alpha \hat\phi_k
%	\right]\partial_\alpha \bar\mu_j
%%\notag\\&\quad
%%	+ \hat\phi_i  A^2 \sum_{j,k} \hat\phi_j (\partial_\alpha \hat\phi_k)(\partial_\alpha \bar\mu_j)  % term 6
%\end{align}
\paragraph{Simplified case}
We next consider a limiting case where there is a solvent phase that always exists in a significant fraction and has a fast mobility.
If we chose the solvent as the $N$-th component, which is removed due to incompressibility, this condition implies $\lambda_i\phi_i \ll \lambda_N\phi_N$ for $i=1,\ldots,N-1$.
To first order, we then find
$
	\bar\Lambda_{ij} \approx \lambda_i \phi_i \delta_{ij}
$
implying the mobility matrix is dominated by the diagonal terms, cross-diffusion is negligible, and the dynamics of the solvent does not affect the behavior of the slow, dilute components.
This choice leads to the following approximate dynamical equations:
\begin{align}
	\partial_t \phi_i \approx 
	\partial_\alpha\left(
		\lambda_i \phi_i \partial_\alpha \bar\mu_i
	\right)
	= 
	\lambda_i (\partial_\alpha\phi_i) (\partial_\alpha \bar\mu_i)
	+ \lambda_i \phi_i \partial_\alpha^2 \bar\mu_i
\end{align}


\subsection{Stochastic dynamics}
The stochastic dynamics are equivalent to the compressible case.
To implement the correct covariance for Kramer's model, we choose the mobility matrix $\Lambda_{ij} = \lambda_0(\phi_i\delta_{ij} - \phi_i\phi_j)$.
Our task is now to find a matrix $O_{ij}$ such that $\sum_k O_{ik}O_{jk} = \bar\Lambda_{ij}$.
This is in principle possible, since the matrix $\bar\Lambda_{ij}$ is positive semidefinite, but we did not yet find a nice expression that we can implement in code.
If it becomes necessary to support stochastic simulations with Kramer's mobility, we might need to perform a Cholesky decomposition at each support point.

\subsection{Special case for two components}

The typical Flory-Huggins free energy for two components is recovered for the choice
\begin{align}
	 w_i &= \begin{pmatrix} 0\\ 0 \end{pmatrix} 
&
	\chi_{ij} &=  \begin{pmatrix} 
	      0 & \chi \\
	      \chi & 0
	   \end{pmatrix}
\end{align}
so that we obtain $\bar w_0 = \chi$, $\bar\chi_{00} = -2\chi$, and $\bar\kappa_{00} = 2\lambda^2\chi$.
This results in 
\begin{align}
	\bar f(\phi, \partial_\alpha\phi) =\frac{\kBT}{\nu}\left[
		\phi\log(\phi) + (1-\phi)\log(1-\phi) + \chi \phi(1-\phi) + \frac{\kappa}{2}(\partial_\alpha\phi)^2
	\right]
\end{align}
and
\begin{align}
	\bar\mu(\phi, \partial_\alpha\phi) = \kBT\left[
		\log(\phi) - \log(1-\phi) + \chi\left(1 - 2\phi\right) - \kappa\partial_\alpha^2\phi
	\right]
	\;.
\end{align}
Note that we here defined the positive quantity $\kappa = \kappa_{00} = 2\lambda^2 \chi$.
Additionally using Kramers model with constant mobilities, $\Lambda_{ij} = \Lambda_0 \phi_i \delta_{ij}$, we recover the familiar form
\begin{align}
	\bar\Lambda &= \Lambda_0  \phi ( 1 - \phi)
\end{align}
for the only relevant mobility coefficient.


\appendix

\section{Extension: unequal molecular volumes}
\textbf{This section is currently outdated (as of 2021-10-14)}

Assuming different molecular volumes $v_i$ for each species, the particle concentration $c_i=c_i/V$ and volume fraction $\phi_i=v_ic_i$ differ. In a lattice model each $i$ particle occupies $c_i$ lattice sites of volume $v_0$, so $v_i=c_iv_0$. In this case the free energy reads

\begin{align}
 F= \int \biggl[ 
\sum_{i=1}^{N} c_i \log\phi_i
+ \sum_{i=1}^{N} w_i c_i
+ \sum_{i,j=0}^{N-1} \frac{ze_{ij}}{2} \phi_i \phi_j
- \sum_{i,j=0}^{N-1} \frac{\lambda_{ij}^2e_{ij}}{2} \partial_\alpha\phi_i \partial_\alpha\phi_j
\biggr] \diff^3 r
\;.
\end{align}
Here the first term is the flory huggins entropy of mixing. $ w_i$ is the internal energy per particle (so it scales with $c_i$). $\phi_i\phi_j$ is the probability for a contact site between $i$ and $j$ z is the number of nearest neighbours and $e_{ij}$ is the energy per contact site. The last term includes the interaction range $\lambda_{ij}$ for the gradient terms.\\
Using $\phi_i = 1-\sum_{j\neq i}\phi_j$ we rewrite the interaction term as

\begin{align}
	\sum_{i,j=0}^{N-1}e_{ij}\phi_i \phi_j &= \sum_i \phi_i e_{ii} + \sum_{i,j=1}^{N-1} \phi_i\phi_j (e_{ij}-e_{ii}),
\\
	\sum_{i,j=0}^{N-1} \phi_i\phi_j (e_{ij}-e_{ii}) &=
		\sum_{i,j>i=0}^{N-1} \phi_i\phi_j (2e{ij}-e_{ii}-e_{jj}) = \frac{1}{2}\sum_{i,j=0}^{N-1} \bar{\chi}_{ij}\phi_i\phi_j\;.
\end{align}
Here $\bar{\chi}_{ij} = (2e_{ij}-e_{ii}-e_{jj})$ and ${\chi}_{ij} = \frac{z}{2}\bar{\chi}_{ij}$ is the interaction parameter discussed above (by definition $\bar{\chi}_{ij}=\bar{\chi}_{ji}$ and $\bar{\chi}_{ii}=0$). The factor $\frac{ze_{ii}}{2}$ can be absorbed in $ w_i$. We apply the same reshifting of the sum to the gradient term and get $\kappa_{ij} = \lambda_{ij} \chi_{ij}$, so the resulting free energy is
\begin{align}
 F[\set{\phi_i}] = \int \biggl[ 
\sum_{i=1}^{N} c_i \log\phi_i
+ \sum_{i=1}^{N} w_i c_i
+ \sum_{i,j=0}^{N-1} \frac{ \chi_{ij}}{2} \phi_i \phi_j
- \sum_{i,j=0}^{N-1} \frac{ \kappa_{ij}}{2} \partial_\alpha\phi_i \partial_\alpha\phi_j
\biggr] \diff^3 r
\;.
\end{align}
Now we replace the $N-1$-th species, just like before. But we have to take care that $\phi_{N-1} = v_{N-1}c_{N-1}$. In this case we get (again $r=N-1$)
\begin{align}
F[\set{\phi_i}] = \int \biggl[ 
\sum_{i=1}^{N-1} c_i \log\phi_i 
+ c_N \log \phi_N
+ \sum_{i=1}^{N-1} w_i c_i
+ \sum_{i,j=0}^{N-1} \frac{\chi_{ij}}{2} \phi_i \phi_j
+ \sum_{i,j=0}^{N-1} \frac{\kappa_{ij}}{2} \partial_\alpha\phi_i \partial_\alpha\phi_j
\biggr] \diff^3 r
\;,
\end{align}
where
\begin{subequations}
	\begin{align}
	w_i &=  w_i - \frac{v_i}{v_{r}} w_N + v_i\chi_{ir}
	\\
	\chi_{ij} &= \chi_{ij} - 2\chi_{ir} 
	\\
	\kappa_{ij} &= 2\lambda_{ir} - \lambda_{ij}\chi_{ij}\chi_{ir} 
	\end{align}
\end{subequations}
are the model parameters after application of constant volume. Now we compute the chemical potential as before using $\mu_i(\Phi,\partial_\alpha\Phi) = \delta F/\delta c_i$ ($\Phi=(\phi_1,...,\phi_{N-1})$)
\begin{align}
\mu_i =
	\log(\phi_i)  - \frac{v_i}{v_N}\log(\phi_N) + 1 - \frac{v_i}{v_N} + w_N
	+ \sum_{j=1}^{N-1}\frac{v_i\chi_{ij}}{2}\phi_j 
	- \sum_{j=1}^{N-1}\frac{v_i\kappa_{ij}}{2}\partial_\alpha^2\phi_j
\end{align}



\bibliographystyle{plain}
\bibliography{../bibliography}

\end{document}
