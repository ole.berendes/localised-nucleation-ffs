'py-phasesep' python package
============================

The `py-phasesep` python package provides methods and classes useful for 
studying phase separation phenomena using phase field methods.



Contents
--------

.. toctree-filt::
    :maxdepth: 4
    :numbered:

    installation
    quickstart/quickstart
    :gallery:examples_gallery/index
    packages/phasesep
 


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
