
# %%
from dask.distributed import Client, LocalCluster
import ffs
import bistable
from numba import njit
import tqdm
import numpy as np

# %%
# stepper function


@njit
def my_step_function(state, dt):
    gamma = 10
    kbt = 1
    m = 1

    x = state[0]
    v = state[1]

    # make potential

    def double_well(x, k=10, a=2):
        energy = 0.25*k*((x-a)**2) * ((x+a)**2)
        force = -k*x*(x-a)*(x+a)
        return energy, force

    return bistable.single_time_step(m, gamma, kbt, double_well, x, v, dt)


# order parameter
@njit
def calc_op(state):
    return state[0]


# %%
# ffs
def ffs_run_all_steps(client, parameters, step_function, calculate_op, initial_state):

    lambda0 = parameters["interfaces"][0]
    max_crossings = parameters["number of crossings"]
    dt = parameters["integration time step"]
    t_max = parameters["max simulation time"]
    save_interval = parameters["save interval"]

    # run initial sampling

    times, states = ffs.solve_initial_trajectory(
        step_function,
        initial_state,
        calculate_op,
        lambda0,
        max_crossings,
        dt,
        t_max,
        save_interval
    )

    new_initial_states = []
    for i in range(1, len(states)):
        prev_op = calc_op(states[i-1])
        op = calc_op(states[i])

        if prev_op < lambda0 and op >= lambda0:
            new_initial_states.append(states[i])

    flux = len(new_initial_states)/times[-1]

    # Cross subsequent interfaces

    probs = []
    for lambd in tqdm.tqdm(parameters["interfaces"][1:]):
        prob, successful_trajs = ffs.sample_next_interface_parallel(
            client,
            step_function,
            new_initial_states,
            calculate_op,
            lambda0,
            lambd,
            max_crossings,
            dt,
            t_max,
            save_interval
        )

        new_initial_states = []
        for traj in successful_trajs:
            new_initial_states.append(traj[-1])

        probs.append(prob)

    return flux, probs


# %%
def main():
    # Start client
    cluster = LocalCluster(n_workers=8)
    client = Client(cluster)

    # run ffs sampling
    iterations = 20
    ffs_params = {
        "interfaces": np.arange(-1.75, 2, 0.25),
        "number of crossings": 100,
        "integration time step": 1e-3,
        "max simulation time": 1e9,
        "save interval": 1e-1
    }

    initial = (-2, 0)
    print("Initialised simulation")

    results = []
    for i in range(iterations):
        first_flux, probs = ffs_run_all_steps(
            client,
            ffs_params,
            my_step_function,
            calc_op,
            initial,
        )
        results.append([first_flux, probs])
        print(f"Computed FFS run {i}")

    results_out = []
    for result in results:
        first_flux = result[0]
        probs = result[1]
        results_out.append([first_flux, probs])

    np.savetxt("020222-SIM01.dat", results_out)
    client.close()
    cluster.close()


if __name__ == "__main__":
    main()
