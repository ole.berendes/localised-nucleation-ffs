"""
Functions for a bistable stochastic system. Potential, force and time stepper.
"""

from numba import njit
import numpy as np


@njit
def single_time_step(m, gamma, kbt, potential, x, v, dt):
    def update_position(x, v, dt):
        return x + v * dt

    def update_velocity(v, F, dt):
        return v + F/m * dt

    def update_random_velocity(v, gamma, dt):
        R = np.random.normal()
        c1 = np.exp(-gamma*dt)
        c2 = np.sqrt(1-c1*c1)*np.sqrt(kbt)
        return c1*v + R*c2

    # B
    _, force = potential(x)
    v = update_velocity(v, force, dt/2)

    # A
    x = update_position(x, v, dt/2)

    # O
    v = update_random_velocity(v, gamma, dt/2)

    # A
    x = update_position(x, v, dt/2)

    # B
    v = update_velocity(v, force, dt/2)

    return x, v


@njit
def double_well_potential(x, k, a):
    energy = 0.25*k*((x-a)**2) * ((x+a)**2)
    force = -k*x*(x-a)*(x+a)
    return energy, force