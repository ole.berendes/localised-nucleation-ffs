# %%
from dask_jobqueue import HTCondorCluster
from dask.distributed import Client
import pde
import phasesep as ps
import shutil

# %% [markdown]
# ## Setting  up Dask cluster

# %%
cluster = HTCondorCluster(
    cores=1,
    memory="5GB",
    disk="10GB",
    processes=1,
    local_directory="$TMPDIR",
    log_directory="/home/ole.berendes/dasklogs",
    job_extra={"+MaxRunTime": 86400}
)

# %%
cluster.scale(1)

# %%
client = Client(cluster)

# %% [markdown]
# ## Setting up the system

# %% [markdown]
# ### Free energy density

# %%
f_scale = 1
f_slope = 0.02
f_offset = 0
kappa = 1
diffusive_mobility = 1
noise_diffusion = 2/30

# %%

f = ps.GinzburgLandau2Components(
    prefactor=f_scale,
    slope=f_slope,
    offset=f_offset
)

# %%
binodal = f.get_binodal()[0][0]
spinodal = f.get_spinodal()[0]
binodal_range = spinodal - binodal

# %% [markdown]
# ### Equation

# %%
eq = ps.CahnHilliardExtendedPDE(
    {
        "free_energy": f,
        "kappa": kappa,
        "mobility": diffusive_mobility,
        "noise_diffusion": noise_diffusion
    }
)

# %% [markdown]
# ## Solve pde

# %% [markdown]
# ### Define grid

# %%
nx, ny = 32, 32
x_start, x_end = -16, 16
y_start, y_end = -16, 16
grid = pde.CartesianGrid(
    bounds=[(x_start, x_end), (y_start, y_end)],
    shape=[nx,ny],
    periodic=True)

# %% [markdown]
# ### Initial state

# %%
phi0 = binodal + 0.2*binodal_range

initial_field = pde.ScalarField.random_normal(
    grid=grid,
    mean=phi0,
    std=phi0*0.1
)

# %%
dt = 1e-3


# %%
def integrate(initial_state, t_end, dt=dt, eq=eq):
    storage = pde.FileStorage("$TMPDIR/trajectory.h5", write_mode="truncate_once")
    stor = storage.tracker()
    plot = pde.PlotTracker(interval=60, output_file="/home/ole.berendes/monitor.png")
    final = eq.solve(
        state=initial_state,
        t_range=t_end,
        tracker=[stor, plot],
        method="explicit",
        dt=dt
    )
    shutil.copy("$TMPDIR/trajectory.h5", "/home/ole.berendes/data/trajectory.h5")
    return final


# %%
client.submit(integrate, initial_field, 1e6)
