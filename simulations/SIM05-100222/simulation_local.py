
# %%
from dask.distributed import Client, LocalCluster
from dask_jobqueue import HTCondorCluster
import ffs
import bistable
from numba import njit
import tqdm
import numpy as np
from time import sleep


"""
FFS of a 1D Langevin equation in a double well potential. The goal is to calculate transition rates and compare them to analytical results to assess th ffs algorithm.
"""

# %%
# stepper function


@njit
def my_step_function(state, dt):
    gamma = 1
    kbt = 1
    m = 1

    x = state[0]
    v = state[1]

    # make potential

    def double_well(x, k=4, a=2):
        energy = 0.25*k*((x-a)**2) * ((x+a)**2)
        force = -k*x*(x-a)*(x+a)
        return energy, force

    return bistable.single_time_step(m, gamma, kbt, double_well, x, v, dt)


# order parameter
@njit
def calc_op(state):
    return state[0]


# %%
def main():
    # Start client
    cluster = LocalCluster(n_workers=34, processes=True, local_directory="/scratch/ole.berendes")
    client = Client(cluster)

    # ffs params
    iterations = 20

    initial = (-2, 0)
    print("Initialised simulation")

    results = []
    for i in range(iterations):
        result = ffs.run_ffs_dask(
            client=client,
            step_function=my_step_function,
            calculate_op=calc_op,
            interfaces=np.linspace(-1.4, 2, 9),
            num_crossings=2000,
            dt=1e-3,
            t_max=1e6,
            save_interval=0.1,
            initial_state=initial
        )
        results.append(result)
        print(f"Computed FFS run {i}")

    results_out_arr = np.array(results)
    np.savetxt("/scratch/ole.berendes/100222-SIM05.txt", results_out_arr)
    client.close()
    cluster.close()


if __name__ == "__main__":
    main()
