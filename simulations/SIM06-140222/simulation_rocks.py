
# %%
from dask.distributed import Client, LocalCluster
import ffs_direct
import bistable
from numba import njit
import numpy as np
from dask_mpi import initialize


"""
FFS of a 1D Langevin equation in a double well potential. The goal is to calculate transition rates and compare them to analytical results to assess th ffs algorithm.
"""

# %%
# stepper function


@njit
def my_step_function(state, dt):
    gamma = 1
    kbt = 1
    m = 1

    x = state[0]
    v = state[1]

    # make potential

    def double_well(x, k=4, a=2):
        energy = 0.25*k*((x-a)**2) * ((x+a)**2)
        force = -k*x*(x-a)*(x+a)
        return energy, force

    return bistable.single_time_step(m, gamma, kbt, double_well, x, v, dt)


# order parameter
@njit
def calc_op(state):
    return state[0]


# %%
def main():
    # Start client
    initialize(local_directory="$TMPDIR")
    client = Client()
    print("Started client")

    # ffs params
    interfaces = np.linspace(-1.5, 1.5, 10)
    iterations = 20
    initial = (-2, 0)
    ncrossings = 5000
    dt = 1e-3
    t_max = 1e5
    save_interval = 0.1
    dirout = "/scratch/ole.berendes"
    
    print("Initialised simulation")

    for i in range(iterations):
        result = ffs_direct.calibration_run(
            client,
            my_step_function,
            calc_op,
            initial,
            interfaces,
            ncrossings,
            dt,
            t_max,
            save_interval,
            dirout
        )
        print(f"Computed FFS run {i}")
        file = open("/home/ole.berendestrials.txt", "a")
        file.write(str(result["trials"]+"\n"))
        file.close()

        file = open("/home/ole.berendes/factors.txt", "a")
        file.write(str(result["flux factors"]+"\n"))
        file.close()

if __name__ == "__main__":
    main()
