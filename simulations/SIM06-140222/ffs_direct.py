"""
Module for runnng direct forward flux sampling.
"""

from audioop import cross
from numba import njit
import random
from dask.distributed import Client, Future
from tqdm import tqdm

# SIMULATION SOLVERS
# ------------------


@njit
def compute_initial_flux(
    step_func,
    initial_state,
    calculate_op,
    lambda0,
    ncrossings,
    dt,
    t_max,
    save_interval,
    fileout
):

    """
    Compute the flux through the first interface

    Parameters
    ----------

    step_func: Function for making one integration time step of the
         underlying simulation. Must take simulation state and step
         size dt as parameters.
    initial_state: Inital state of th simulation in the A basin
    calculate_op: Function to calculate the order parameter.
        Must take simulation state as an argument.
    lambda_0: Position of the first interface.
    ncrossings: Number of crossings over lambda_0 to gather
        before simulation is stopped.
    dt: Integration time step.
    t_max: Maximum simulation time until abortion.
    save_interval: Simulation time interval for svaing the
        simulation state
    fileout: File to which the whole trajectory should
        be saved on disk.

    Returns
    -------

    Dictionary with entries
    - flux: Flux through lambda_0
    - crossing_states: Saved states after trajectory crossed
        lambda_0 in forward direction.

    """

    # initial values
    crossings = 0
    state = initial_state
    t = 0
    times = [t]
    results = [state]
    crossing_states = []
    last_save = t
    previous_op = calculate_op(initial_state)

    # collect n crossings of lambda_0
    while crossings < ncrossings:
        state = step_func(state, dt)
        t = t + dt

        if t >= t_max:
            raise StopIteration("Computation timed out")

        elif t - last_save >= save_interval:
            results.append(state)
            times.append(t)
            last_save = t
            op = calculate_op(state)

            if previous_op < lambda0 and op >= lambda0:
                crossings += 1
                crossing_states.append(state)

            previous_op = calculate_op(state)

    flux = len(crossing_states)/times[-1]

    return flux, crossing_states


@njit
def solve_until_stop(
    step_func,
    initial_state,
    calculate_op,
    op_min,
    op_max,
    dt,
    t_max,
    save_interval
):

    """
    Propagate simulation until the order parameter reaches the next
    interface or returns to the A state.

    Parameters
    ----------

    step_func: Function for making one integration time step of the
        underlying simulation. Must take simulation state and step
        size dt as parameters.
    initial_state: Inital state of th simulation in the A basin
    calculate_op: Function to calculate the order parameter. Must
        take simulation state as an argument.
    op_min: Minimum value of order parameter at which the simulation
        is stopped. Corresponds to the A basin.
    op_max: Maximum value of order parameter, i. e. the next
        interface
    dt: Integration time step.
    t_max: Maximum simulation time until abortion.
    save_interval: Simulation time interval for svaing the
        simulation state

    Returns
    -------

    Simulation trajectory

    """

    state = initial_state
    t = 0
    trajectory = [state]
    last_save = t
    previous_op = calculate_op(initial_state)

    while True:
        state = step_func(state, dt)
        t = t + dt

        if t > t_max:
            raise StopIteration("Computation timed out.")

        elif t - last_save >= save_interval:
            trajectory.append(state)
            last_save = t
            op = calculate_op(state)

            if previous_op < op_max and op >= op_max:
                break

            elif previous_op > op_min and op <= op_min:
                break

            previous_op = calculate_op(state)

    return trajectory


# CALIBRATION RUN
# ---------------


def calibration_run(
    client: Client,
    step_function,
    calculate_op,
    inital_state,
    interfaces,
    ncrossings,
    dt,
    t_max,
    save_interval,
    directory_out
):

    """
    Do a calibration run from A to B

    Parameters
    ----------

    client: Dask client which parallel computations are submitted to
    step_function: Function for making one integration time step of the
        underlying simulation. Must take simulation state and step
        size dt as parameters.
    calculate_op: Function to calculate the order parameter. Must
        take simulation state as an argument.
    inital_state: Inital state of the simulation in the A basin.
    interfaces: Positons of the interfaces along the reaction coordinate
    ncrossings: Number of desired interface crossings.
    dt: Integration time step.
    t_max: Maximum simulation time until abortion.
    save_interval: Simulation time interval for svaing the
        simulation state
    directory_out: Directory in which the successful trajectories
        should be saved.

    Returns
    -------

    Dictionary with entries:
    - trials: List with the number of fired trials at each interface
    - flux factors: List of the crossing probabilities for all interfaces.
        The first entry is the flux through lambda_0
    """

    def sample_next_interface(
        initial_states,
        op_min,
        op_max,
        ncrossings,
        directory_out,
        client: Client = client,
        step_func=step_function,
        calculate_op=calculate_op,
        dt=dt,
        t_max=t_max,
        save_interval=save_interval,
    ):

        """
        Sample successful paths to next interface

        Parameters
        ----------

        initial_states: possible inital states of th simulations. For
            each run, one is randomly chosen
        op_min: Minimum value of order parameter at which the simulation
            is stopped. Corresponds to the A basin.
        op_max: Maximum value of order parameter, i. e. the next
            interface
        ncrossings: Number of desired interface crossings.

        Returns
        -------

        Dictionary with entries:
        - trials: Total number of trials
        - crossing probability: Probability for crossing the next
            interface
        - successful states: Final states of the successful simulation
        """

        def compute_successful_traj(
            step_func=step_func,
            inital_states=initial_states,
            calculate_op=calculate_op,
            op_min=op_min,
            op_max=op_max,
            dt=dt,
            t_max=t_max,
            save_interval=save_interval
        ):

            """
            Run simulations from randomly chosen initial states until one reaches the next interface

            Returns
            -------

            Dictionary with entries:
            - trials: Number of trials until a success is reached
            - final state: Final state of the successful simulation

            """

            success = False
            trials = 0
            while success is False:
                trials += 1
                initial_state = random.choice(inital_states)
                states = solve_until_stop(
                    step_func,
                    initial_state,
                    calculate_op,
                    op_min, op_max,
                    dt,
                    t_max,
                    save_interval
                )

                op = calculate_op(states[-1])

                if op >= op_max:
                    success = True

            return {"trials": trials, "final state": states[-1]}

        futures = []
        for i in range(ncrossings):
            future = client.submit(compute_successful_traj)
            futures.append(future)

        results = client.gather(futures)
        trials = 0
        successful_states = []
        for result in results:
            trials = trials + result["trials"]
            successful_states.append(result["final state"])
        crossing_prob = len(successful_states)/trials
        return_dict = {
            "trials": trials,
            "crossing probability": crossing_prob,
            "successful states": successful_states
        }

        return return_dict

    # sample the A basin and comput inital flux
    file = directory_out+"/initial_trajectory.hdf5"
    future: Future
    future = client.submit(
        compute_initial_flux,
        step_function,
        inital_state,
        calculate_op,
        interfaces[0],
        ncrossings,
        dt,
        t_max,
        save_interval,
        file
    )
    result = future.result()
    initial_flux = result[0]
    new_initial_states = result[1]

    # Sample other interfaces
    all_trials = []
    factors = [initial_flux]
    for interface in tqdm(interfaces[1:]):
        result_dict = sample_next_interface(
            initial_states=new_initial_states,
            op_min=interfaces[0],
            op_max=interface,
            ncrossings=ncrossings,
            directory_out=directory_out
        )
        all_trials.append(result_dict["trials"])
        factors.append(result_dict["crossing probability"])
        new_initial_states = result_dict["successful states"]

    result_dict = {
        "trials": all_trials,
        "flux factors": factors
    }

    return result_dict


# MAIN FFS RUN
# ------------


def complete_run(
    client: Client,
    step_function,
    calculate_op,
    inital_state,
    interfaces,
    trials,
    dt,
    t_max,
    save_interval,
    directory_out
):

    """
    Do a full run from A to B

    Parameters
    ----------

    client: Dask client which parallel computations are submitted to
    step_function: Function for making one integration time step of the
        underlying simulation. Must take simulation state and step
        size dt as parameters.
    calculate_op: Function to calculate the order parameter. Must
        take simulation state as an argument.
    inital_state: Inital state of the simulation in the A basin.
    interfaces: Positons of the interfaces along the reaction coordinate
    trials: Number of trials to fire for each interface. First entry is the number of crossings for initial sampling.
    dt: Integration time step.
    t_max: Maximum simulation time until abortion.
    save_interval: Simulation time interval for svaing the
        simulation state
    directory_out: Directory in which the successful trajectories
        should be saved.

    Returns
    -------

    List with initial flux (first entry) and subsequent crossing probabilities
    """

    def sample_next_interface(
        initial_states,
        op_min,
        op_max,
        trials,
        directory_out,
        client: Client = client,
        step_func=step_function,
        calculate_op=calculate_op,
        dt=dt,
        t_max=t_max,
        save_interval=save_interval,
    ):

        """
        Sample successful paths to next interface

        Parameters
        ----------

        initial_states: possible inital states of th simulations. For
            each run, one is randomly chosen
        op_min: Minimum value of order parameter at which the simulation
            is stopped. Corresponds to the A basin.
        op_max: Maximum value of order parameter, i. e. the next
            interface
        trials: Number of trials to fire.

        Returns
        -------

        Dictionary with entries:
        - crossing probability: Probability for crossing the next
            interface
        - successful states: Final states of the successful simulation
        """

        def check_success(
            trajectory,
            op_max=op_max,
            calculate_op=calculate_op
        ):
            op = calculate_op(trajectory[-1])
            if op >= op_max:
                return trajectory[-1]
            else:
                return None

        futures = []
        for i in range(trials):
            random_choice = random.choice(initial_states)
            future = client.submit(
                solve_until_stop,
                step_func,
                random_choice,
                calculate_op,
                op_min,
                op_max,
                dt,
                t_max,
                save_interval
            )
            futures.append(future)
        success_futures = client.map(check_success, futures)
        successes = client.gather(success_futures)
        successes = list(filter(None, successes))
        crossing_prob = len(successes)/trials

        return {"crossing probability": crossing_prob,
            "successful states": successes}

    # sample the A basin and comput inital flux
    file = directory_out+"/initial_trajectory.hdf5"
    future: Future
    future = client.submit(
        compute_initial_flux,
        step_function,
        inital_state,
        calculate_op,
        interfaces[0],
        trials[0],
        dt,
        t_max,
        save_interval,
        file
    )
    result = future.result()
    factors = [result[0]]
    new_initial_states = result[1]

    # compute subsequent crossing probabilities
    for i in tqdm(range(1,len(interfaces))):
        result_dict = sample_next_interface(
            new_initial_states,
            interfaces[0],
            interfaces[i],
            trials[i],
            directory_out
        )
        factors.append(result_dict["crossing probability"])
        new_initial_states = result_dict["successful states"]

    return factors
