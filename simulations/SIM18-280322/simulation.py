import pde
import pickle
import sys
import phasesep as ps
import os

# free energy density
f_scale = 1
f_slope = -0.02
f_offset = 0
f = ps.GinzburgLandau2Components(
    prefactor=f_scale,
    slope=f_slope,
    offset=f_offset
)

# binodal and spinodal
binodal = f.get_binodal()[0][0]
spinodal = f.get_spinodal()[0]
binodal_range = spinodal - binodal

# pde
kappa = 1
diffusive_mobility = 1
noise_diffusion = 2/30
eq = ps.CahnHilliardExtendedPDE(
    {
        "free_energy": f,
        "kappa": kappa,
        "mobility": diffusive_mobility,
        "noise_diffusion": noise_diffusion
    }
)
eq.cache_rhs = True

# grid
length_scale = 1
nx, ny = 32, 32
x_start, x_end = -nx/2*length_scale, nx/2*length_scale
y_start, y_end = -ny/2*length_scale, ny/2*length_scale
grid = pde.CartesianGrid(
    bounds=[(x_start, x_end), (y_start, y_end)],
    shape=[nx, ny],
    periodic=True)

# initial state
phi0 = binodal + 0.85*binodal_range
initial_field = pde.ScalarField.random_normal(
    grid=grid,
    mean=phi0,
    std=phi0*0.1
)

if __name__ == "__main__":
    dataloc = sys.argv[1]
    os.makedirs(dataloc, exist_ok=True)
    storage = pde.MemoryStorage(field_obj=initial_field)
    stor = storage.tracker()
    plot = pde.PlotTracker(output_file=dataloc+"/monitor.png")
    eq.solve(
        state=initial_field,
        t_range=1e6,
        dt=1e-3,
        tracker=[stor, plot],
        method="explicit"
    )
    file = open(dataloc+"/traj.pickle", "wb")
    pickle.dump(storage, file)
    file.close()