import pde
import simulation
import numpy as np
import random
import json
import sys
from concurrent.futures import Executor
from mpi4py.futures import MPIPoolExecutor

def simulation_explor_run(initial_state, t):
    """Should return op_values of trajectory"""
    opar: simulation.DropletMaterialTracker
    opar, _ = simulation.run(
        initial_state=initial_state,
        op_min=-1,
        op_max=1e3,
        ncrossings=1e3,
        t_end=t
    )
    return opar.op_history


def basin_simulation(initial_state, op_max, ncrossings):
    """Returns list of states that crossed op_max and flux through op_max"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=-1,
        op_max=op_max,
        ncrossings=ncrossings,
        t_end=1e6
    )
    states = opar.crossed_states
    time = storage.times[-1]
    flux = len(states)/time

    return flux, states


def trial_simulation(initial_state, op_max, op_min):
    """Returns final state if op_max was crossed and None otherwise"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=op_min,
        op_max=op_max,
        ncrossings=1,
        t_end=1e3
    )
    if opar.success:
        state = opar.crossed_states[0]
    else:
        state = None

    return state


def exploration_run(
    executor: Executor,
    initial_state, t_basin_run, t_trial_runs, lambda_B
):

    interfaces = []
    probabilities = []
    trials = 1000
    actual_trials = trials

    # do basin run to determine lambda_A and lambda_0
    ops = simulation_explor_run(initial_state, t_basin_run)
    lam_A = np.std(ops)
    interfaces.append(np.percentile(ops, 95))
    print(f"Set lambda_A at {lam_A} and lambda_0 at {interfaces[0]}.")
    sys.stdout.flush()

    # do actual basin simulation
    initial_flux, next_init_states = basin_simulation(
        initial_state, interfaces[0], 100
    )
    print(f"Flux over lambda_0: {initial_flux}.")
    sys.stdout.flush()

    while True:
        # fire large number of trial runs, each running for t_trial_runs
        # calculate max. of ops for each run
        # determine some percentile of all max. op values
        # use this as next interface value
        init_state_it = random.choices(next_init_states, k=actual_trials)
        t_it = [t_trial_runs]*actual_trials
        futures = executor.map(
            simulation_explor_run,
            init_state_it,
            t_it,
            chunksize=50
        )
        max_ops = []
        for ops in list(futures):
            max_ops.append(np.max(ops))
        next_interface = np.percentile(max_ops, 80)
        if next_interface >= lambda_B:
            break
        elif next_interface > interfaces[-1]:
            interfaces.append(next_interface)
            print(f"Set next interface at {next_interface}")
            sys.stdout.flush()

            # perform actual TP sampling to next interface
            op_max_it = [interfaces[-1]]*actual_trials
            op_min_it = [lam_A]*actual_trials
            futures = executor.map(
                trial_simulation,
                init_state_it,
                op_max_it,
                op_min_it,
                chunksize=50
            )
            next_init_states = []
            for state in list(futures):
                if state is not None:
                    next_init_states.append(state)
            probability = len(next_init_states)/actual_trials
            probabilities.append(probability)
            print(
                f"Transition probability from {interfaces[-2]} \
                to {interfaces[-1]} is {probability}."
            )
            sys.stdout.flush()
            actual_trials = trials
        else:
            actual_trials += trials

    return {
        "interfaces": interfaces,
        "probabilities": probabilities,
        "initial flux": initial_flux
    }


if __name__ == "__main__":
    print("Initiating Executor")
    sys.stdout.flush()
    with MPIPoolExecutor(max_workers=3) as executor:
        initial_state = simulation.initial_field
        print("Starting exploratory run")
        sys.stdout.flush()
        info_dict = exploration_run(
            executor, initial_state,
            2e3, 1e2,
            80
        )
        text = json.dumps(info_dict)
        file = open("result.json", 'w')
        file.write(text)
        file.close()