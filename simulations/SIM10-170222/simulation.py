import phasesep as ps
import pde
import tracker
import numpy as np
import json
import sys

# make free energy density
f_scale = 1
f_slope = -0.02
f_offset = 0

f = ps.GinzburgLandau2Components(
    prefactor=f_scale,
    slope=f_slope,
    offset=f_offset
)

# get binodal and spinodal
binodal = f.get_binodal()[0][0]
spinodal = f.get_spinodal()[0]
binodal_range = spinodal - binodal

# make pde
kappa = 1
diffusive_mobility = 1
noise_diffusion = 2/30

eq = ps.CahnHilliardExtendedPDE(
    {
        "free_energy": f,
        "kappa": kappa,
        "mobility": diffusive_mobility,
        "noise_diffusion": noise_diffusion
    }
)
eq.cache_rhs = True

# define grid
length_scale = 1

nx, ny = 32, 32
x_start, x_end = -nx/2*length_scale, nx/2*length_scale
y_start, y_end = -ny/2*length_scale, ny/2*length_scale
grid = pde.CartesianGrid(
    bounds=[(x_start, x_end), (y_start, y_end)],
    shape=[nx,ny],
    periodic=True)

# make initial state
phi0 = binodal + 0.25*binodal_range

initial_field = pde.ScalarField.random_normal(
    grid=grid,
    mean=phi0,
    std=phi0*0.1
)

# simulation
dt = 1e-3


def run_inital_sampling(
    initial_state,
    t_end,
    filename_base,
    monitorfile,
    dt=dt,
    eq=eq
):



    opar = tracker.DropletMaterialTracker(
        op_max=3.03,
        op_min=-1,
        ncrossings=500
    )
    storage = pde.MemoryStorage(
        field_obj=initial_state,
        write_mode="truncate_once"
    )
    stor = storage.tracker()
    plot = pde.PlotTracker(interval=100, output_file=monitorfile)
    eq.solve(
        state=initial_state,
        t_range=t_end,
        tracker=[stor, plot, opar],
        method="explicit",
        dt=dt
    )
    traj_file = filename_base+"_trajectory.npz"
    states_file = filename_base+"_crossed_states.npz"
    np.savez_compressed(traj_file, *storage.data)
    np.savez_compressed(states_file, *opar.crossed_states)


# main


def main():
    jobid = sys.argv[1]
    filename_base = f"/home/ole.berendes/data/170222-SIM10/SIM10_{jobid}"
    monitor = f"/home/ole.berendes/monitors/monitor_{jobid}.png"
    run_inital_sampling(
        initial_state=initial_field,
        t_end=1e5,
        filename_base=filename_base,
        monitorfile=monitor
    )


if __name__ == "__main__":
    main()