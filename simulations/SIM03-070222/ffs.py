"""
Module for runnng direct forward flux sampling.
"""

from numba import njit
import random
from dask.distributed import Client


@njit
def solve_initial_trajectory(
    step_func,  # function to make integration time step
    initial_state,  # initial state of the simulation
    calculate_op,  # function to caculate order parameter
    lambda0,  # first interface
    max_crossings,  # number of interface crossings to record before sim. stops
    dt,  # integration time step
    t_max,  # max. simulation time
    save_interval  # interval at which to record simulation state
):
    crossings = 0
    state = initial_state
    t = 0
    times = [t]
    results = [state]
    last_save = t
    prev_op = calculate_op(initial_state)

    while crossings < max_crossings:
        state = step_func(state, dt)
        t = t + dt

        if t >= t_max:
            raise StopIteration("Computation timed out")

        elif t - last_save >= save_interval:
            results.append(state)
            times.append(t)
            last_save = t
            op = calculate_op(state)

            if prev_op < lambda0 and op >= lambda0:
                crossings += 1

            prev_op = calculate_op(state)

    return times, results


@njit
def solve_until_stop(
    step_func,  # function to make integration time step
    initial_state,  # initial state of the simulation
    calculate_op,  # function to caculate order parameter
    op_min,  # min. order param. at which to stop sim.
    op_max,  # max. order param. at which to stop sim.
    dt,  # integration time step
    t_max,  # max. simulation time
    save_interval  # interval at which to record simulation state
):
    state = initial_state
    t = 0
    times = [t]
    results = [state]
    last_save = t
    prev_op = calculate_op(initial_state)

    while True:
        state = step_func(state, dt)
        t = t + dt

        if t > t_max:
            raise StopIteration("Computation timed out.")

        elif t - last_save >= save_interval:
            results.append(state)
            times.append(t)
            last_save = t
            op = calculate_op(state)

            if prev_op < op_max and op >= op_max:
                break

            elif prev_op > op_min and op <= op_min:
                break

            prev_op = calculate_op(state)

    return times, results


def compute_successful_traj(
    initial_states,
    step_func,
    calculate_op,
    op_min,
    op_max,
    dt,
    t_max,
    save_interval
):

    success = False
    trials = 0
    while success is False:
        trials += 1
        initial_state = random.choice(initial_states)
        _, states = solve_until_stop(
            step_func,
            initial_state,
            calculate_op,
            op_min, op_max,
            dt,
            t_max,
            save_interval
        )

        op = calculate_op(states[-1])

        if op >= op_max:
            success = True

    return trials, states


def sample_next_interface(
    step_func,  # function to make integration time step
    initial_states,  # initial states of the simulations
    calculate_op,  # function to caculate order parameter
    op_min,  # min. order param. at which to stop sim.
    op_max,  # max. order param. at which to stop sim.
    num_crossings,  # number of interface crossings to reach
    dt,  # integration time step
    t_max,  # max. simulation time
    save_interval  # interval at which to record simulation state
):

    # start simulations until one success is reached
    # repeat until num_crossings successes are reached

    results = []
    for i in range(num_crossings):
        result = compute_successful_traj(
            initial_states,
            step_func,
            calculate_op,
            op_min,
            op_max,
            dt,
            t_max,
            save_interval
        )
        results.append(result)

    trials = 0
    successful_trajs = []
    for item in results:
        trials = trials + item[0]
        successful_trajs.append(item[1])

    crossing_prob = len(successful_trajs)/trials

    return crossing_prob, successful_trajs


def sample_next_interface_parallel(
    client: Client,  # dask client to submit to
    step_func,  # function to make integration time step
    initial_states,  # initial states of the simulations
    calculate_op,  # function to caculate order parameter
    op_min,  # min. order param. at which to stop sim.
    op_max,  # max. order param. at which to stop sim.
    num_crossings,  # number of interface crossings to reach
    dt,  # integration time step
    t_max,  # max. simulation time
    save_interval  # interval at which to record simulation state
):
    # Start n_crossings computations of a successful trajectory in parallel

    futures = []
    for i in range(num_crossings):
        future = client.submit(
            compute_successful_traj,
            initial_states,
            step_func,
            calculate_op,
            op_min,
            op_max,
            dt,
            t_max,
            save_interval,
            pure=False
        )
        futures.append(future)

    results = client.gather(futures)

    trials = 0
    successful_trajs = []
    for result in results:
        new_trials = result[0]
        traj = result[1]
        trials = trials + new_trials
        successful_trajs.append(traj)

    # compute probability of crossing the next interface

    crossing_prob = len(successful_trajs)/trials

    return crossing_prob, successful_trajs