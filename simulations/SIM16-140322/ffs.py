import pde
import simulation
import numpy as np
import random
import json
import sys
from concurrent.futures import Executor
from mpi4py.futures import MPIPoolExecutor


def basin_simulation(initial_state, op_max, ncrossings):
    """Returns list of states that crossed op_max and flux through op_max"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=-1,
        op_max=op_max,
        ncrossings=ncrossings,
        t_end=1e6
    )
    states = opar.crossed_states
    time = storage.times[-1]
    flux = len(states)/time

    return flux, states


def trial_simulation(initial_state, op_max, op_min):
    """Returns final state if op_max was crossed and None otherwise"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=op_min,
        op_max=op_max,
        ncrossings=1,
        t_end=1e3
    )
    if opar.success:
        state = opar.crossed_states[0]
    else:
        state = None

    return state


def ffs_run(
    executor: Executor,
    initial_state,
    interfaces,
    trials,
    min_successes
):
    # do basin run to determine flux
    initial_flux, next_init_states = basin_simulation(
        initial_state, interfaces[0], trials
    )
    print(f"Flux over lambda_0: {initial_flux}.")
    sys.stdout.flush()

    probabilities = []

    # fire n trial runs at each interface
    for i in range(1, len(interfaces)):
        nsuccess = 0
        success_states = []
        total_trials = 0

        while nsuccess < min_successes:
            # choose random initial states
            init_state_it = random.choices(next_init_states, k=trials)
            # run n trial runs
            op_max_it = [interfaces[i]]*trials
            op_min_it = [interfaces[0]]*trials
            futures = executor.map(
                trial_simulation,
                init_state_it,
                op_max_it,
                op_min_it,
                chunksize=50
            )
            for state in list(futures):
                if state is not None:
                    success_states.append(state)
                    nsuccess += 1
            total_trials += trials

        probability = len(next_init_states)/trials
        probabilities.append(probability)
        next_init_states = success_states
        print(
            f"Fired {total_trials}, of which {len(success_states)} were successful.\
            Transition probability from {interfaces[i-1]}\
            to {interfaces[i]} is {probability}."
        )
        sys.stdout.flush()
  
    return {
        "probabilities": probabilities,
        "initial flux": initial_flux
    }

if __name__ == "__main__":
    print("Initiating Executor")
    sys.stdout.flush()
    with MPIPoolExecutor(max_workers=19) as executor:
        interfaces = np.linspace(5, 80, 10)
        trials = 1000
        min_successes = 100
        initial_state = simulation.initial_field
        print("Starting ffs run")
        sys.stdout.flush()
        info_dict = ffs_run(
            executor, initial_state,
            interfaces, trials,
            min_successes
        )
        text = json.dumps(info_dict)
        file = open("result.json", 'w')
        file.write(text)
        file.close()
