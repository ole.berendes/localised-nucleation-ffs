from requests import delete
import pde
import simulation
import numpy as np
import random
import json
import sys
import pickle
import uuid
import os
import shutil
from concurrent.futures import Executor
from mpi4py.futures import MPIPoolExecutor


def basin_simulation(initial_state, op_max, ncrossings, dataloc):
    """Returns list of states that crossed op_max and flux through op_max"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=-1,
        op_max=op_max,
        ncrossings=ncrossings,
        t_end=1e6,
    )
    states = opar.crossed_states
    times = opar.times
    ids = []
    for t in times:
        traj = storage.extract_time_range(t)
        id = str(uuid.uuid4())
        ids.append(id)
        filename = os.path.join(dataloc, id + ".pkl")
        with open(filename, mode="wb") as file:
            pickle.dump(traj, file)
    t_total = storage.times[-1]
    flux = len(states) / t_total
    if len(ids) != len(states):
        raise ValueError("id list and states have unequal length.")

    return flux, states, ids


def trial_simulation(initial_state, op_max, op_min, dataloc):
    """Returns final state if op_max was crossed and None otherwise"""
    opar: simulation.DropletMaterialTracker
    storage: pde.MemoryStorage
    opar, storage = simulation.run(
        initial_state=initial_state,
        op_min=op_min,
        op_max=op_max,
        ncrossings=1,
        t_end=1e6,
    )
    if opar.success:
        state = opar.crossed_states[0]
        id = str(uuid.uuid4())
        filename = os.path.join(dataloc, id + ".pkl")
        with open(filename, mode="wb") as file:
            pickle.dump(storage, file)
        print("Successful path.")
        sys.stdout.flush()
    else:
        state = None
        id = None
        print("Failed path.")
        sys.stdout.flush()

    return state, id


def ffs_run(
    executor: Executor,
    initial_state,
    interfaces,
    trials,
    min_successes,
    dataloc,
):

    # # make database
    # db_path = os.path.join(dataloc, "connections.db")
    # con = sqlite3.connect(db_path)
    # cur = con.cursor()
    # # create table
    # cur.execute('''
    #     CREATE TABLE trajectories
    #     (prev_id text, id text)
    # ''')

    # do basin run to determine flux
    shutil.rmtree(dataloc, ignore_errors=True)
    os.makedirs(dataloc)
    initial_flux, next_init_states, ids = basin_simulation(
        initial_state, interfaces[0], trials, dataloc
    )
    print(f"Flux over lambda_0: {initial_flux}.")
    sys.stdout.flush()

    # # insert ids into db
    # for id in ids:
    #     cur.execute(
    #         """INSERT INTO trajectories VALUES (?, ?)'""",
    #         ("NaN", id)
    #     )

    probabilities = []
    success_ids = ids

    # fire n trial runs at each interface
    for i in range(1, len(interfaces)):
        prev_ids = success_ids
        delete(prev_ids)
        nsuccess = 0
        success_states = []
        success_ids = []
        total_trials = 0

        while nsuccess < min_successes:
            # choose random initial states
            indices = range(len(next_init_states))
            rand_indices = random.choices(indices, k=trials)
            rand_init_state_it = [next_init_states[i] for i in rand_indices]
            # run n trial runs
            op_max_it = [interfaces[i]] * trials
            op_min_it = [interfaces[0]] * trials
            dataloc_it = [dataloc] * trials
            futures = executor.map(
                trial_simulation,
                rand_init_state_it,
                op_max_it,
                op_min_it,
                dataloc_it,
                chunksize=10,
            )
            results = list(futures)
            for j in range(len(rand_indices)):
                state = results[j][0]
                id = results[j][1]
                if state is not None:
                    success_states.append(state)
                    # cur.execute(
                    #     """INSERT INTO trajectories VALUES (?, ?)'""",
                    #     (prev_ids[rand_indices[j]], id)
                    # )
                    success_ids.append(id)
                    nsuccess += 1
            total_trials += trials
            print(f"{total_trials} trials and {nsuccess} successes.")
            sys.stdout.flush()

        probability = nsuccess / trials
        probabilities.append(probability)
        next_init_states = success_states
        print(
            f"Fired {total_trials}, of which {len(success_states)} were successful.\
            Transition probability from {interfaces[i-1]}\
            to {interfaces[i]} is {probability}."
        )
        sys.stdout.flush()

    return {"probabilities": probabilities, "initial flux": initial_flux}


if __name__ == "__main__":
    dataloc = sys.argv[1]
    print("Initiating Executor")
    sys.stdout.flush()
    with MPIPoolExecutor(max_workers=19) as executor:
        interfaces = [10**1.17, *np.logspace(1.3, 1.9, 9)]
        trials = 100
        min_successes = 10
        initial_state = simulation.initial_field
        print("Starting ffs run")
        sys.stdout.flush()
        info_dict = ffs_run(
            executor, initial_state, interfaces, trials, min_successes, dataloc
        )
        text = json.dumps(info_dict)
        print(text)
        sys.stdout.flush()
        file = open("result.json", "w")
        file.write(text)
        file.close()
