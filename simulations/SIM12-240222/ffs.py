import pde
from pde.trackers.intervals import IntervalData
from pde.trackers.base import InfoDict, FinishedSimulation
from pde.fields import FieldBase
from skimage.measure import label, regionprops
from skimage.filters import gaussian
import numpy as np
import phasesep as ps
import os
from mpi4py import MPI
from mpi4py.futures import MPIPoolExecutor
import shutil


class DropletMaterialTracker(pde.trackers.TrackerBase):

    def __init__(
        self, op_max, op_min, ncrossings,
        interval: IntervalData = 1
    ):
        """
        Tracker for the amount of material inside a droplet for FFS
        
        Parameters
        ----------
        op_max: Order paremeter at which simulation is successfully finished
        op_min: Order parameter at which simulation is unsuccessfully finished
        ncrssings: No. of crossings over op_max trequired to successfully finish simulation
        interval: Interval at which the tracker is periodically called 
        """
        self.cell_volumes = None
        self.op_history = []
        self.max = op_max
        self.min = op_min
        self.ncrossings = ncrossings
        self.count = 0
        self.crossed_states = []
        self.success = False
        super().__init__(interval)

    def calc_op(self, field: FieldBase):
        if self.cell_volumes is None:
            self.cell_volumes = field.grid.cell_volumes
        # Multily data with cell volumes
        im = field.data*self.cell_volumes
        # Gaussian filter
        im = gaussian(field.data, sigma=2)
        # search for regions
        label_image = label(im > (np.mean(im) + 2 * np.std(im)))
        a = regionprops(label_image, im)

        N = np.zeros(len(a))
        i = 0
        for item in a:
            N[i] = item.mean_intensity*item.area
            i += 1
        try:
            op = np.max(N)
        except ValueError:
            op = 0
        return op

    def initialize(self, field: FieldBase, info: InfoDict = None) -> float:
        self.prev_op = self.calc_op(field)
        if self.prev_op >= self.max:
            raise ValueError(f"OP of initial state ({self.prev_op}) is larger\
            than max. value of {self.max}.")
        return super().initialize(field, info)

    def handle(self, field: FieldBase, t: float) -> None:
        op = self.calc_op(field)
        self.op_history.append(op)
        if op >= self.max and self.prev_op < self.max:
            self.crossed_states.append(field)
            self.count += 1
        if self.count >= self.ncrossings:
            self.success = True
            raise FinishedSimulation
        self.prev_op = op
        return super().handle(field, t)


def run_trajectory(
    initial_state: pde.ScalarField,
    op_min: float,
    op_max: float,
    ncrossings: float,
    t_end: float,
    dt: float,
    eq: pde.PDEBase,
    fname: str,
) -> DropletMaterialTracker:
    opar = DropletMaterialTracker(
        op_max, op_min, ncrossings
    )
    storage = pde.FileStorage(fname)
    stor = storage.tracker()
    eq.solve(
        state=initial_state,
        t_range=t_end,
        dt=dt,
        tracker=[opar, stor],
        method="explicit"
    )

    return opar


# free energy density
f_scale = 1
f_slope = -0.02
f_offset = 0
f = ps.GinzburgLandau2Components(
    prefactor=f_scale,
    slope=f_slope,
    offset=f_offset
)

# binodal and spinodal
binodal = f.get_binodal()[0][0]
spinodal = f.get_spinodal()[0]
binodal_range = spinodal - binodal

# pde
kappa = 1
diffusive_mobility = 1
noise_diffusion = 2/30
eq = ps.CahnHilliardExtendedPDE(
    {
        "free_energy": f,
        "kappa": kappa,
        "mobility": diffusive_mobility,
        "noise_diffusion": noise_diffusion
    }
)
eq.cache_rhs = True

# grid
length_scale = 1
nx, ny = 32, 32
x_start, x_end = -nx/2*length_scale, nx/2*length_scale
y_start, y_end = -ny/2*length_scale, ny/2*length_scale
grid = pde.CartesianGrid(
    bounds=[(x_start, x_end), (y_start, y_end)],
    shape=[nx, ny],
    periodic=True)

# initial state
phi0 = binodal + 0.85*binodal_range
initial_field = pde.ScalarField.random_normal(
    grid=grid,
    mean=phi0,
    std=phi0*0.1
)

# integration step
dt = 1e-3

# FFS parameters
op_min = 1.75
min_crossings = 10
trials = 10
lambdas = np.logspace(
    start=np.log(3),
    stop=np.log(80),
    num=10,
    base=np.e
)

# directories
datadir = "/home/ole.berendes/data/SIM12"
tmpdir = "/tmp"


# def initial run
def inital_run(
    initial_state,
    fname,
    t_end=1e5
):
    tmpfile = os.path.join(tmpdir, "traj.h5")
    os.makedirs(os.path.dirname(fname), exist_ok=True)
    op_tracker: DropletMaterialTracker
    op_tracker = run_trajectory(
        initial_state=initial_state,
        op_min=-1,
        op_max=lambdas[0],
        ncrossings=min_crossings,
        t_end=t_end,
        dt=dt,
        eq=eq,
        fname=tmpfile
    )
    shutil.copy(tmpfile, fname)
    return op_tracker.crossed_states


# def subs sampling
def run_until_stop(
    initial_state,
    fname,
    op_min,
    op_max,
    ncrossings=1,
    t_end=2e3,
    dt=1e-3,
    eq=eq
):
    op_tracker: DropletMaterialTracker
    op_tracker = run_trajectory(
        initial_state=initial_state,
        op_min=op_min,
        op_max=op_max,
        ncrossings=ncrossings,
        t_end=t_end,
        dt=dt,
        eq=eq,
        fname=fname
    )

    return op_tracker.crossed_states[0]


if __name__ == "__main__":
    with MPIPoolExecutor() as executor:
        print("initial concentration:", phi0)
        print("Interface positions:", lambdas)

        # equilibrate sample
        opar = DropletMaterialTracker(10, -1, 1)
        while opar.calc_op(initial_field) >= lambdas[0]:
            initial_field = pde.ScalarField.random_normal(
                grid=grid,
                mean=phi0,
                std=phi0*0.1
            )

        # Run initial sampling
        print("Sampling start basin.")
        fname = os.path.join(datadir, "initial_run", "traj.h5")
        future = executor.submit(
            inital_run, initial_field, fname
        )
        crossed_states = future.result()
        print(f"collected {len(crossed_states)} crossings.")
        # Run subsequent samplings