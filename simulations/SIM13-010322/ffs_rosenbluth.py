from mpi4py.futures import MPIPoolExecutor
from typing import Sequence
import random


def simulation_run_base(initial_state, op_min, op_max, t_end):
    """Placeholder for a simulation function. Returns final state if op_max is
    reached and None if op_min is reached or simulation runs for t_max time
    units."""
    pass


def proceed_to_next_interface(
    executor: MPIPoolExecutor,
    simulation_func: simulation_run_base,
    initial_states: Sequence,
    op_min: float,
    op_max: float,
    ntrials: int,
    ncrossings: int
):
    crossings = 0
    trials = 0
    initial_state = random.choice(initial_states)
    initial_state_it = [initial_state]*ntrials
    op_min_it = [op_min]*ntrials
    op_max_it = [op_max]*ntrials
    successful_states = []

    while crossings < ncrossings:
        futures = executor.map(
            simulation_func,
            initial_state_it,
            op_min_it,
            op_max_it
        )
        # filter Nones
        successful_states.extend(list(filter(None, list(futures))))
        trials += ntrials
        crossings = len(successful_states)

    info_dict = {
        "trials": trials,
        "crossings": crossings,
        "probability": crossings/trials
    }

    return successful_states, info_dict
