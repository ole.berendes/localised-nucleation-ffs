import phasesep as ps
import pde
from ffs_rosenbluth import proceed_to_next_interface
from simulation import run_trajectory
import numpy as np
import os
import shutil

def uniquify(path):
    filename, extension = os.path.splitext(path)
    counter = 1

    while os.path.exists(path):
        path = filename + " (" + str(counter) + ")" + extension
        counter += 1

    return path

def extendpath(path, extension):
    filename, suffix = os.path.split()


# free energy density
f_scale = 1
f_slope = -0.02
f_offset = 0
f = ps.GinzburgLandau2Components(
    prefactor=f_scale,
    slope=f_slope,
    offset=f_offset
)

# binodal and spinodal
binodal = f.get_binodal()[0][0]
spinodal = f.get_spinodal()[0]
binodal_range = spinodal - binodal

# pde
kappa = 1
diffusive_mobility = 1
noise_diffusion = 2/30
eq = ps.CahnHilliardExtendedPDE(
    {
        "free_energy": f,
        "kappa": kappa,
        "mobility": diffusive_mobility,
        "noise_diffusion": noise_diffusion
    }
)
eq.cache_rhs = True

# grid
length_scale = 1
nx, ny = 32, 32
x_start, x_end = -nx/2*length_scale, nx/2*length_scale
y_start, y_end = -ny/2*length_scale, ny/2*length_scale
grid = pde.CartesianGrid(
    bounds=[(x_start, x_end), (y_start, y_end)],
    shape=[nx, ny],
    periodic=True)

# initial state
phi0 = binodal + 0.85*binodal_range

# integration step
dt = 1e-3

# FFS parameters
op_min = 1.75
crossings_first_interface = 100
min_crossings = 10
lambdas = np.logspace(
    start=np.log(3),
    stop=np.log(80),
    num=10,
    base=np.e
)
trials = [10]*len(lambdas)



def initial_run(
    initial_state,
    fname
):
    tmpfile = os.path.join("/tmp", uniquify("traj.h5"))
    opar = run_trajectory(
        initial_state=initial_state,
        op_min=-1,
        op_max=lambdas[0],
        ncrossings=crossings_first_interface,
        t_end=1e5,
        dt=dt,
        eq=eq,
        fname=tmpfile
    )
    if opar.success:
        os.makedirs(os.path.dirname(fname), exist_ok=True)
        shutil.copy(tmpfile, fname)
        os.remove(tmpfile)
    else:
        raise ValueError(f"success flag is {opar.success}")

    return opar.crossed_states


def run_until_stop(
    *args
):
    pass
