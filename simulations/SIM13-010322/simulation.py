import pde
import numpy as np
from pde.trackers.base import InfoDict, IntervalData, FinishedSimulation
from pde import FieldBase
from skimage.filters import gaussian
from skimage.measure import label, regionprops


class DropletMaterialTracker(pde.trackers.TrackerBase):

    def __init__(
        self, op_max, op_min, ncrossings,
        interval: IntervalData = 1
    ):
        """
        Tracker for the amount of material inside a droplet for FFS
        
        Parameters
        ----------
        op_max: Order paremeter at which simulation is successfully finished
        op_min: Order parameter at which simulation is unsuccessfully finished
        ncrssings: No. of crossings over op_max trequired to successfully finish simulation
        interval: Interval at which the tracker is periodically called 
        """
        self.cell_volumes = None
        self.op_history = []
        self.max = op_max
        self.min = op_min
        self.ncrossings = ncrossings
        self.count = 0
        self.crossed_states = []
        self.success = False
        super().__init__(interval)

    def calc_op(self, field: FieldBase):
        if self.cell_volumes is None:
            self.cell_volumes = field.grid.cell_volumes
        # Multily data with cell volumes
        im = field.data*self.cell_volumes
        # Gaussian filter
        im = gaussian(field.data, sigma=2)
        # search for regions
        label_image = label(im > (np.mean(im) + 2 * np.std(im)))
        a = regionprops(label_image, im)

        N = np.zeros(len(a))
        i = 0
        for item in a:
            N[i] = item.mean_intensity*item.area
            i += 1
        try:
            op = np.max(N)
        except ValueError:
            op = 0
        return op

    def initialize(self, field: FieldBase, info: InfoDict = None) -> float:
        self.prev_op = self.calc_op(field)
        if self.prev_op >= self.max:
            raise ValueError(f"OP of initial state ({self.prev_op}) is larger\
            than max. value of {self.max}.")
        return super().initialize(field, info)

    def handle(self, field: FieldBase, t: float) -> None:
        op = self.calc_op(field)
        self.op_history.append(op)
        if op >= self.max and self.prev_op < self.max:
            self.crossed_states.append(field)
            self.count += 1
        if self.count >= self.ncrossings:
            self.success = True
            raise FinishedSimulation
        self.prev_op = op
        return super().handle(field, t)


def run_trajectory(
    initial_state: pde.ScalarField,
    op_min: float,
    op_max: float,
    ncrossings: float,
    t_end: float,
    dt: float,
    eq: pde.PDEBase,
    fname: str,
) -> DropletMaterialTracker:
    """
    Run a simulation trajectory until op_max has been crossed ncrossings times.
    """
    opar = DropletMaterialTracker(
        op_max, op_min, ncrossings
    )
    storage = pde.FileStorage(fname)
    stor = storage.tracker()
    eq.solve(
        state=initial_state,
        t_range=t_end,
        dt=dt,
        tracker=[opar, stor],
        method="explicit"
    )

    return opar
