import matplotlib.pyplot as plt
import numpy as np
import os

dir = os.path.dirname(__file__)


# implicitly calculate fixed points c*(b)
def b(c):
    return 2*c**3 - 3*c**2 + c


# segment c between extrema of b(c)
c1 = np.linspace(-0, 0.5-1/(2*np.sqrt(3)))
c2 = np.linspace(0.5-1/(2*np.sqrt(3)), 0.5+1/(2*np.sqrt(3)))
c3 = np.linspace(0.5+1/(2*np.sqrt(3)), 1)

#c alculate b
b1 = b(c1)
b2 = b(c2)
b3 = b(c3)

# plot c w/ respect to b
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'mathtext.fontset': 'dejavuserif'})
fig, ax = plt.subplots()
ax.plot(b1, c1, color='tab:blue')
ax.plot(b2, c2, color='tab:blue', linestyle='--')
ax.plot(b3, c3, color='tab:blue')
ax.set_xlabel("Free energy density slope $b$")
ax.set_ylabel("Fixed points $\\phi^*$")
fname = os.path.join(dir, "output/stability_analysis_binary.pdf")
plt.savefig(fname)