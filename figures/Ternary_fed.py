# %%
import phasesep
import numpy as np
import matplotlib.pyplot as plt

# %%
''' determine free energy density from interaction parameters '''
# The internal energies correspond to the slope of the two component free energy
wa, wb, wc = 0.,0.,0.
ws = np.array([wa,wb,wc])

# Chi is the interaction between droplet forming species B and the other two species
# The interaction is described by a matrix now, because each species interacts with every other
chi = 3.
chis = np.array([[0,chi,0],[chi,0,chi],[0,chi,0]])

# Define the free energy density for N=3 components using the interaction chi and internal energies w
f1 = phasesep.FloryHugginsNComponents(num_comp=3,chis=chis, internal_energies=ws)
f1.expression

# %%
fed = [f1.free_energy([0, x]) for x in np.linspace(0,1)]

# %%
figa, ax = plt.subplots()
phi1_values = np.linspace(0, 0.1, 5)
phi2_values = np.linspace(0, 1, 100)
for phi1 in phi1_values:
    fed = [f1.free_energy([phi1, x]) for x in phi2_values]
    ax.plot(phi2_values, fed, label=f"$\\phi_A={np.around(phi1, 3)}$")
ax.set_ylabel("free energy density")
ax.set_xlabel("concentration $\\phi_B$")
ax.legend()
plt.savefig("output/ternary_fed.pdf")

# %%



