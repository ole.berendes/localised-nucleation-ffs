from pde.trackers.base import TrackerBase, IntervalData, InfoDict, FinishedSimulation
from pde.fields.base import FieldBase
from skimage.filters import gaussian
from skimage.measure import label, regionprops
from typing import Callable
from copy import copy


class InterfaceCrossingTracker(TrackerBase):

    def __init__(self, compute_op_func: Callable, op_max: float, op_min: float, interval: IntervalData = 1) -> None:
        self.compute_op_func = compute_op_func
        self.op_max = op_max
        self.op_min = op_min
        self.success = False
        super().__init__(interval)

    def handle(self, field: FieldBase, t: float) -> None:
        op = self.compute_op_func(field)

        if op >= self.op_max:
            self.success = True
            raise FinishedSimulation("Reached next interface")

        elif op <= self.op_min:
            raise FinishedSimulation("Returned to first interface")


class FirstCrossingTracker(TrackerBase):

    def __init__(self, compute_op_func: Callable, lambda_0: float, stop_count: int, interval: IntervalData = 1) -> None:
        self.compute_op_func = compute_op_func
        self.lambda_0 = lambda_0
        self.stop_count = stop_count
        self.counter = 0
        self.crossing_times = list()
        self.prev_op = None,
        super().__init__(interval)

    def initialize(self, field: FieldBase, info: InfoDict = None) -> float:
        self.prev_op = self.compute_op_func(field)
        return super().initialize(field, info)

    def handle(self, field: FieldBase, t: float) -> None:
        op = self.compute_op_func(field)

        if self.counter == self.stop_count:
            raise FinishedSimulation("Reached max no. of crossings")

        elif op >= self.lambda_0 and self.prev_op < self.lambda_0:
            self.crossing_times.append(t)
            self.counter += 1

        self.prev_op = copy(op)
