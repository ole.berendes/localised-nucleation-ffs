from typing import Dict, Any, Callable, List, Sequence
from ffs.op_trackers import FirstCrossingTracker, InterfaceCrossingTracker
from pde import ScalarField, PDEBase, MemoryStorage
import dask
from random import choice


def compute_single_traj(func: Callable, *args):
    return func(*args)


class Controller:

    def __init__(self, pde: PDEBase, compute_op_func: Callable,
                 parameters: Dict[str, Any]) -> None:

        self.pde = pde
        self.params = parameters
        self.op_func = compute_op_func

    def run_single_traj_step(self, op_max: float, storage: MemoryStorage):

        # set important variables
        initial_state = storage[-1]
        op_min = self.params["interfaces"][0]
        t_start = storage.times[-1]
        t_max = t_start + self.params["maximum single simulation time"]
        dt = self.params["integration time step"]

        #  create storage and trackers
        # exclude last data point which is the start of the new simulation
        storage = storage.extract_time_range(storage.times[-1])
        storage.write_mode = "append"
        store = storage.tracker()  # storage tracker for storing fields
        cross = InterfaceCrossingTracker(self.op_func, op_max, op_min)

        # run simulation
        self.pde.solve(state=initial_state, t_range=(
            t_start, t_max), dt=dt, tracker=[store, cross], method="explicit")
        return (storage, cross.success)

    def run_initial_sampling(
                self,
                initial_state: ScalarField,
                max_crossings: int,
                op_max: float
            ) -> Sequence[MemoryStorage]:

        # set important variables
        t_max = self.params["maximum single simulation time"]
        dt = self.params["integration time step"]

        # create storage and trackers
        storage = MemoryStorage(field_obj=initial_state, write_mode="append")
        store = storage.tracker()
        cross = FirstCrossingTracker(self.op_func, op_max, max_crossings)

        # run simulation
        self.pde.solve(state=initial_state, t_range=t_max, dt=dt, tracker=[
                                     store, cross], method="explicit")

        print(
            f"Collected {len(cross.crossing_times)} crossings\
                 of the first interface at lambda={op_max}.")

        # create different trajectories ending at lambda_0
        def make_trajectories(times: List[float], base_traj: MemoryStorage):
            traj_list = []

            for t in times:
                new_traj = base_traj.extract_time_range(t+dt)
                new_traj.write_mode = "append"
                traj_list.append(new_traj)

            return traj_list

        return make_trajectories(cross.crossing_times, storage)

    def transition_to_next_interface(
            self,
            prev_trajectories: Sequence[MemoryStorage],
            op_max: float,
            n_traj: int) -> Sequence[MemoryStorage]:

        # check if prev. trajectories exist
        if len(prev_trajectories) == 0:
            raise IndexError(
                "No successful trajectories in previous iteration")

        print(f"Starting {n_traj} simulations")

        # compute n simulations with dask
        new_trajectories = []
        for i in range(n_traj):
            prev_traj = dask.delayed(choice)(prev_trajectories)
            result = dask.delayed(self.run_single_traj_step)(op_max, prev_traj)
            new_trajectories.append(result)

        new_trajectories = dask.compute(*new_trajectories)

        # keep only successful trajectories
        successful_trajectories = []
        for tuple in new_trajectories:
            traj, success = tuple

            if success is True:
                successful_trajectories.append(traj)

        print(
            f"{len(successful_trajectories)} simulations ended\
                 successfully at lambda={op_max}.")
        return successful_trajectories

    def run(self):

        pass
